package com.ocd.hashtagsurfer.stream.instagram;

import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import com.ocd.hashtagsurfer.model.instagram.UserInfo;
import com.ocd.hashtagsurfer.stream.SurferActivity;
import com.ocd.hashtagsurfer.util.Constants;
import com.ocd.hashtagsurfer.util.Parser;

import java.io.IOException;
import java.net.HttpURLConnection;

/**
 * Created by rakz on 07/11/15.
 */
public class UserInfoService extends AbstractInstagramService {

    private static final String USER_INFO = "https://api.instagram.com/v1/users/self/?access_token=";
    private static final String LOG_TAG = UserInfoService.class.getSimpleName();

    public UserInfoService() {
        super(LOG_TAG);
    }

    @Override
    protected void onHandleIntent(final Intent intent) {

        if (!intent.hasExtra(SurferActivity.ACCESS_TOKEN_KEY_INSTAGRAM) || !intent.hasExtra(SurferActivity.PROFILE_RECEIVER)) {
            throw new IllegalArgumentException("Mandatory params missing!");
        }

        UserInfo response = null;
        int resultCode = Constants.RESULT_CODE_OK;
        try {
            HttpURLConnection urlConnection = connectToURL(USER_INFO + intent.getStringExtra(SurferActivity.ACCESS_TOKEN_KEY_INSTAGRAM));

            int status = urlConnection.getResponseCode();
            Log.i(LOG_TAG, "Tag search returned with status: " + status);
            switch (status) {
                case HttpURLConnection.HTTP_OK:
                    // Parse response
                    response = Parser.extractUserInfo(readStream(urlConnection.getInputStream()));
                    urlConnection.disconnect();
                    break;
                default:
                    Log.e(LOG_TAG, "Problem is: " + urlConnection.getResponseMessage() + " ");
                    if (checkIfExpired(urlConnection)) {
                        resultCode = Constants.RESULT_CODE_ACCESS_TOKEN_EXPIRED;
                    }
                    break;
            }

        } catch (IOException e) {
            Log.e(LOG_TAG, "Failed to talk with Instagram", e);
            resultCode = Constants.RESULT_CODE_NETWORK_ERROR;
        } finally {
            ResultReceiver receiver = intent.getParcelableExtra(SurferActivity.PROFILE_RECEIVER);
            Bundle result = new Bundle();
            result.putSerializable(SEARCH_RESULT, response);
            receiver.send(resultCode, result);
        }

    }

}
