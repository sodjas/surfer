package com.ocd.hashtagsurfer.stream.instagram;

import android.app.IntentService;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by rakz on 25/01/15 Copyright OCDTeam.
 */
public abstract class AbstractInstagramService extends IntentService {

    public static final String SEARCH_RESULT = "SEARCH_RESULT";

    public AbstractInstagramService(String serviceName) {
        super(serviceName);
    }


    protected boolean checkIfExpired(HttpURLConnection urlConnection) throws IOException {

        if (readStream(urlConnection.getErrorStream()).contains("OAuthAccessTokenException")) {
            Log.e(AbstractInstagramService.class.getSimpleName(), "Access token expired!");
            return true;
        }
        return false;
    }

    /**
     * @param urlString URL to connect to.
     * @return HttpsURLConnection which then can be evaluated.
     * @throws IOException if something goes wrong with the communication.
     */
    protected HttpURLConnection connectToURL(String urlString) throws IOException {
        URL url = new URL(urlString);
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
//        Log.i(getClass().getSimpleName(), "Querying with: " + url);
        urlConnection.setUseCaches(false);
        urlConnection.setAllowUserInteraction(false);
        urlConnection.connect();
        return urlConnection;
    }

    /**
     * @param stream InputStream to be read.
     * @return Content
     * @throws IOException
     */
    protected String readStream(InputStream stream) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(stream));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line + "\n");
        }
        br.close();
        //Log.i(LOG_TAG, "Response content: "+sb.toString());
        return sb.toString();
    }
}
