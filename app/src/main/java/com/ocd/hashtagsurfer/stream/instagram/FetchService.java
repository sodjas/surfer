package com.ocd.hashtagsurfer.stream.instagram;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.text.MessageFormat;

import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.io.FileUtils;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.ResultReceiver;
import android.util.Log;

import com.ocd.hashtagsurfer.stream.MediaDetailsActivity;
import com.ocd.hashtagsurfer.stream.StreamFragment;
import com.ocd.hashtagsurfer.util.Constants;

/**
 * @author rakz
 * @author barab
 */
public class FetchService extends AbstractInstagramService {

    private static final String LOG_TAG = NearbyService.class.getSimpleName();

	/***/
	private static final int FULL_DOWNLOAD_PERCENTAGE = 100;

	/** Constant for fetching buffer length. */
	private static final int BUFFER_LENGTH = 51200;

	/**  */
	private static final int REQUESTED_RANGE_NOT_SATISFIABLE = 416;

	/** Constant name for progress temporary folder. */
	private static final String PROGRESS_FOLDER = "progress/";

	/** Constant name for completed installer folder. */
	private static final String COMPLETED_FOLDER = "completed/";

    public static final String MOVIE_ON_SD = "MOVIE_ON_SD";

    /** Temporary file during downloading installer. */
	private static File inProgress;

	/** Final file for installer. */
	private static File completed;

	/**
	 * 
	 */
	public FetchService() {
		super(FetchService.class.getSimpleName());
	}

	@Override
	protected void onHandleIntent(final Intent intent) {
        if (intent == null
                || !intent.hasExtra(MediaDetailsActivity.MOVIE_URL_KEY)
                || !intent.hasExtra(MediaDetailsActivity.SAVE_TYPE_KEY)
                || !intent.hasExtra(StreamFragment.TAG_KEY)
                || !intent.hasExtra(MediaDetailsActivity.FETCH_RECEIVER)) {
            throw new IllegalArgumentException("Mandatory params missing!");
        }

        String url = intent.getStringExtra(MediaDetailsActivity.MOVIE_URL_KEY);

		Log.d(LOG_TAG, MessageFormat.format("Start fetching from {0}", url));

		final File movie = fetchMovie(url);
        ResultReceiver receiver = intent.getParcelableExtra(MediaDetailsActivity.FETCH_RECEIVER);
        Bundle result = new Bundle();
        result.putSerializable(MediaDetailsActivity.SAVE_TYPE_KEY, intent.getSerializableExtra(MediaDetailsActivity.SAVE_TYPE_KEY));
        if (movie != null) {
            File movieOnSD = finalizeInstaller(movie, intent.getStringExtra(StreamFragment.TAG_KEY));
            if (movieOnSD != null) {
                result.putString(MOVIE_ON_SD, movieOnSD.getAbsolutePath());
                receiver.send(Constants.RESULT_CODE_OK, result);
            } else {
                receiver.send(Constants.RESULT_CODE_NETWORK_ERROR, result);
            }
        } else {
            receiver.send(Constants.RESULT_CODE_NETWORK_ERROR, result);
        }
	}

	/**
	 * Fetch movie from a given server address.
	 * 
	 * @param url url path of movie file, which should be download.
	 * @return whether fetch was successful.
	 */
	private File fetchMovie(final String url) {

		BufferedInputStream input = null;
		RandomAccessFile output = null;
		File movie = null;

        String[] segments = url.split("/");
        String fileName = segments[segments.length-1];

		try {

			if (inProgress == null) {
				inProgress = new File(getFilesDir(), PROGRESS_FOLDER);
				inProgress.mkdirs();
			}

			if (completed == null) {
				completed = new File(getFilesDir(), COMPLETED_FOLDER);
				completed.mkdirs();
			}

			final HttpURLConnection httpURLConnection = connectToURL(url);
			if (httpURLConnection == null) {
				Log.e(LOG_TAG, "Failed to download file!");
				return null;
			}

			movie = new File(inProgress, fileName);
			if (movie.exists()) {
                Log.e(LOG_TAG, "File exists try to continue!");
				httpURLConnection.setAllowUserInteraction(true);
				httpURLConnection.setRequestProperty("Range", "bytes=" + movie.length() + "-");
			} else {
				FileUtils.cleanDirectory(inProgress);
                Log.e(LOG_TAG, "File doesn't exist starting clean download!");
			}

			if (!movie.exists() && httpURLConnection.getResponseCode() == HttpsURLConnection.HTTP_OK
					|| movie.exists() && httpURLConnection.getResponseCode() == HttpsURLConnection.HTTP_PARTIAL) {

				final String connectionField = httpURLConnection.getHeaderField("content-range");
				long downloadedSize = 0;
				if (connectionField != null) {
					final String[] connectionRanges = connectionField.substring("bytes=".length()).split("-");
					downloadedSize = Long.valueOf(connectionRanges[0]);
                    Log.d(LOG_TAG, "Able to continue from: " + downloadedSize + "/" + connectionRanges[1].split("/")[1] + " bytes!");
				} else if (connectionField == null && movie.exists()) {
                    Log.e(LOG_TAG, "Failed to get status, starting over clean!");
					movie.delete();
				}

				final long fileLength = httpURLConnection.getContentLength() + downloadedSize;
				input = new BufferedInputStream(httpURLConnection.getInputStream());
				output = new RandomAccessFile(movie, "rw");
				output.seek(downloadedSize);

				final byte[] data = new byte[BUFFER_LENGTH];
				int count = 0;
				int progress = 0;
                Log.d(LOG_TAG, "Downloading " + fileName + "...");
				while ((count = input.read(data, 0, BUFFER_LENGTH)) != -1 && progress != FULL_DOWNLOAD_PERCENTAGE) {
					downloadedSize += count;
					output.write(data, 0, count);
					progress = (int) (downloadedSize * FULL_DOWNLOAD_PERCENTAGE / fileLength);
				}

                Log.d(LOG_TAG, "Progress finished with " + progress + "% (" + downloadedSize + "/" + fileLength + ")");
				// Safety check
				if (progress != FULL_DOWNLOAD_PERCENTAGE) {
                    Log.d(LOG_TAG, "It seems that file is not complete, better to delete it!");
					movie.delete();
					return null;
				}
			} else {
                Log.e(LOG_TAG, "Error during request(" + httpURLConnection.getResponseCode() + "):"
						+ httpURLConnection.getResponseMessage());
				if (httpURLConnection.getResponseCode() == REQUESTED_RANGE_NOT_SATISFIABLE) {
                    Log.e(LOG_TAG, "Problem with range, deleting movie file!");
					movie.delete();
				}
			}
			httpURLConnection.disconnect();
		} catch (final MalformedURLException e) {
            Log.e(LOG_TAG, "Bad URL",e);
			return null;
		} catch (final IOException e) {
            Log.e(LOG_TAG, "General IO error", e);
			return null;
		} finally {
            Log.d(LOG_TAG, "Closing streams if needed!");
			try {
				if (input != null) {
					input.close();
				}
				if (output != null) {
					output.close();
				}
			} catch (final IOException e) {
                Log.e(LOG_TAG, "General IO error", e);
			}
		}

		return movie;
	}

	/**
	 * Check, whether the requested installer movie is downloaded.
	 * 
	 * @param movie temporary installer file
	 * @return true if confirmation was successful, else return false
	 */
	private File finalizeInstaller(final File movie, String hashtag) {

		// Move file temporary installer file to completed folder
        File movies = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_MOVIES);
        File savedFile = null;
        try {
            File hashTag = new File(movies, hashtag);
            hashTag.mkdirs();
            savedFile = new File(hashTag, movie.getName());
            if (savedFile.exists()) {
                movie.delete();
                return savedFile;
            }
            FileUtils.moveFile(movie, savedFile);
		} catch (final IOException e) {
            Log.e(LOG_TAG, "General IO error", e);
			return null;
		}
		return savedFile;
	}

	/**
	 * Check, whether installer apk is downloaded successfully.
	 * 
	 * @param context android context
	 * @param fileName name of the installer
	 * @return true, if installer is downloaded completely, else false.
	 */
	private boolean isMovieDownloaded(final Context context, final String fileName) {
		return new File(context.getFilesDir() + "/" + COMPLETED_FOLDER, fileName).exists();
	}

	/**
	 * Get directory path, where there completed downloads are stored.
	 * 
	 * @param context android context.
	 * @return complete dir
	 */
	private String getCompleteDirectoryPath(final Context context) {
		return context.getFilesDir() + "/" + COMPLETED_FOLDER;
	}

}