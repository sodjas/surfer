package com.ocd.hashtagsurfer.stream;

import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.applidium.shutterbug.FetchableImageView;
import com.ocd.hashtagsurfer.R;
import com.ocd.hashtagsurfer.model.instagram.MediaItem;

import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rakz on 28/12/14 Copyright OCDTeam.
 */
public class StreamFragmentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final SoftReference<StreamFragment> host;

    private final List<MediaItem> mDataSet = new ArrayList<>();

    private final int densityDpi;

    private final int reqWidth;

    public StreamFragmentAdapter(StreamFragment host) {
        super();
        this.host = new SoftReference<>(host);
        DisplayMetrics metrics = host.getResources().getDisplayMetrics();
        this.densityDpi = metrics.densityDpi;
        this.reqWidth = metrics.widthPixels / StreamFragment.SPAN_COUNT;
    }

    public void setItems(List<MediaItem> data) {
        if (!mDataSet.isEmpty()) {
            int itemrange = mDataSet.size();
            mDataSet.clear();
            notifyItemRangeRemoved(0, itemrange);
        }
        appendItems(data);
    }

    public void appendItems(List<MediaItem> data) {
        int startIndex = mDataSet.size();
        mDataSet.addAll(data);
        notifyItemRangeChanged(startIndex, mDataSet.size());
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                                            .inflate(R.layout.grid_item, null));
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final StreamFragmentAdapter.ViewHolder viewHolder = (ViewHolder) holder;
        MediaItem item = mDataSet.get(position);
        viewHolder.typeOfMedia.setVisibility(item.getType()
                                                 .equals(MediaItem.MEDIA_VIDEO_TYPE) ? View.VISIBLE : View.INVISIBLE);

        switch (densityDpi) {
            case DisplayMetrics.DENSITY_LOW:
            case DisplayMetrics.DENSITY_MEDIUM:
            case DisplayMetrics.DENSITY_HIGH:
                viewHolder.mImageView.setImage(item.getImages()
                                                   .getThumbnail()
                                                   .getUrl(), reqWidth, reqWidth);
                break;
            default:
                viewHolder.mImageView.setImage(item.getImages()
                                                   .getLow_resolution()
                                                   .getUrl(), reqWidth, reqWidth);
                break;
        }

        viewHolder.mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (host.get() != null) {
                    host.get().startMediaDetails(viewHolder.progressBar, position);
                }
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mDataSet.get(position).getType() == null ? 0 : 1;
    }

    public MediaItem getItemAt(int position) {
        return mDataSet.get(position);
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view placeHolder
    public static class ViewHolder extends RecyclerView.ViewHolder {

        public FetchableImageView mImageView;

        public ImageView typeOfMedia;

        public ProgressBar progressBar;

        public ViewHolder(View v) {
            super(v);
            mImageView = (FetchableImageView) v.findViewById(R.id.imageView);
            typeOfMedia = (ImageView) v.findViewById(R.id.type_of_media);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        }
    }

}
