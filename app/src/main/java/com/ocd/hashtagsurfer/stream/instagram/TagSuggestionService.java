package com.ocd.hashtagsurfer.stream.instagram;

import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import com.ocd.hashtagsurfer.model.instagram.TagSuggestionResponse;
import com.ocd.hashtagsurfer.stream.StreamFragment;
import com.ocd.hashtagsurfer.stream.SurferActivity;
import com.ocd.hashtagsurfer.util.Constants;
import com.ocd.hashtagsurfer.util.Parser;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;

/**
 * Created by rakz on 22/02/15 Copyright OCDTeam.
 */
public class TagSuggestionService extends AbstractInstagramService {


    private static final String SEARCH_TAG_SUGGESTION = "https://api.instagram.com/v1/tags/search?q=";
    private static final String TOKEN_SUFFIX = "&access_token=";

    private static final String LOG_TAG = TagSuggestionService.class.getSimpleName();

    public TagSuggestionService() {
        super(TagSuggestionService.class.getSimpleName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        if (intent == null || (!intent.hasExtra(StreamFragment.TAG_KEY))) {
            throw new IllegalArgumentException("Mandatory params missing!");
        }

        if (!intent.hasExtra(SurferActivity.ACCESS_TOKEN_KEY_INSTAGRAM)
                || !intent.hasExtra(StreamFragment.STREAM_RECEIVER)) {
            throw new IllegalArgumentException("Mandatory params missing!");
        }

        TagSuggestionResponse response = null;
        int resultCode = Constants.RESULT_CODE_OK;
        HttpURLConnection urlConnection = null;
        try {
            String hashTag = intent.getStringExtra(StreamFragment.TAG_KEY);
            urlConnection = connectToURL(SEARCH_TAG_SUGGESTION + hashTag + TOKEN_SUFFIX
                    + intent.getStringExtra(SurferActivity.ACCESS_TOKEN_KEY_INSTAGRAM));

            int status = urlConnection.getResponseCode();
            Log.i(LOG_TAG, "Tag search returned with status: " + status);
            switch (status) {
                case HttpURLConnection.HTTP_OK:
                    // Parse response
                    response = Parser.extractTagSuggestionResults(readStream(urlConnection.getInputStream()));
                    urlConnection.disconnect();
                    break;
                default:
                    Log.e(LOG_TAG, "Problem is: " + urlConnection.getResponseMessage() + " ");
                    if (checkIfExpired(urlConnection)) {
                        resultCode = Constants.RESULT_CODE_ACCESS_TOKEN_EXPIRED;
                    }
                    break;
            }

        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Bad URL", e);
            resultCode = Constants.RESULT_CODE_PROTOCOL_ERROR;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Failed to talk with Instagram", e);
            resultCode = Constants.RESULT_CODE_NETWORK_ERROR;
        } finally {
            ResultReceiver receiver = intent.getParcelableExtra(StreamFragment.STREAM_RECEIVER);
            Bundle result = new Bundle();
            result.putSerializable(SEARCH_RESULT, response);
            result.putSerializable(SurferActivity.REFRESH_TYPE_KEY, intent.getSerializableExtra(SurferActivity.REFRESH_TYPE_KEY));
            receiver.send(resultCode, result);
        }

    }
}
