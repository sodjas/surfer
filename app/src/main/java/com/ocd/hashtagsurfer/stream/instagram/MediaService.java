package com.ocd.hashtagsurfer.stream.instagram;

import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import com.ocd.hashtagsurfer.model.instagram.MediaSearchResponse;
import com.ocd.hashtagsurfer.model.instagram.SearchResponse;
import com.ocd.hashtagsurfer.stream.StreamFragment;
import com.ocd.hashtagsurfer.stream.SurferActivity;
import com.ocd.hashtagsurfer.util.Constants;
import com.ocd.hashtagsurfer.util.Parser;

import java.io.IOException;
import java.net.HttpURLConnection;

/**
 * Created by rakz on 20/02/15 Copyright OCDTeam.
 */
public class MediaService extends AbstractInstagramService {

    private static final String SEARCH_MEDIA = "https://api.instagram.com/v1/media/shortcode/";
    private static final String ACCESS_TOKEN_SUFFIX = "?access_token=";
    private static final String LOG_TAG = MediaService.class.getSimpleName();

    public MediaService() {
        super(MediaService.class.getSimpleName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        if (intent == null || (!intent.hasExtra(StreamFragment.MEDIA_KEY))) {
            throw new IllegalArgumentException("Mandatory params missing!");
        }

        if (!intent.hasExtra(SurferActivity.ACCESS_TOKEN_KEY_INSTAGRAM)
                || !intent.hasExtra(StreamFragment.STREAM_RECEIVER)) {
            throw new IllegalArgumentException("Mandatory params missing!");
        }

        int resultCode = Constants.RESULT_CODE_OK;
        MediaSearchResponse response = null;
        try {
            HttpURLConnection urlConnection =
                    connectToURL(SEARCH_MEDIA+intent.getStringExtra(StreamFragment.MEDIA_KEY)+ACCESS_TOKEN_SUFFIX+
                           intent.getStringExtra(SurferActivity.ACCESS_TOKEN_KEY_INSTAGRAM));

            int status = urlConnection.getResponseCode();
            Log.i(LOG_TAG, "Tag search returned with status: " + status);
            switch (status) {
                case HttpURLConnection.HTTP_OK:
                    // Parse response
                    response = Parser.extractMediaResults(readStream(urlConnection.getInputStream()));
                    urlConnection.disconnect();
                    break;
                default:
                    Log.e(LOG_TAG, "Problem is: " + urlConnection.getResponseMessage() + " ");
                    if (checkIfExpired(urlConnection)) {
                        resultCode = Constants.RESULT_CODE_ACCESS_TOKEN_EXPIRED;
                    }
                    break;
            }

        } catch (IOException e) {
            Log.e(LOG_TAG, "Failed to talk with Instagram", e);
            resultCode = Constants.RESULT_CODE_NETWORK_ERROR;
        } finally {
            ResultReceiver receiver = intent.getParcelableExtra(StreamFragment.STREAM_RECEIVER);
            Bundle result = new Bundle();
            result.putSerializable(SEARCH_RESULT, response);
            receiver.send(resultCode, result);
        }

    }
}
