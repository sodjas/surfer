package com.ocd.hashtagsurfer.stream;

import android.app.SearchManager;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.database.MatrixCursor;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.provider.BaseColumns;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.applidium.shutterbug.utils.ShutterbugManager;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.ocd.hashtagsurfer.R;
import com.ocd.hashtagsurfer.app.SurferApplication;
import com.ocd.hashtagsurfer.authorization.instagram.InstaAuthorizeActivity;
import com.ocd.hashtagsurfer.model.instagram.MediaItem;
import com.ocd.hashtagsurfer.model.instagram.MediaSearchResponse;
import com.ocd.hashtagsurfer.model.instagram.SearchResponse;
import com.ocd.hashtagsurfer.model.instagram.TagSuggestion;
import com.ocd.hashtagsurfer.model.instagram.TagSuggestionResponse;
import com.ocd.hashtagsurfer.stream.instagram.MediaService;
import com.ocd.hashtagsurfer.stream.instagram.NearbyService;
import com.ocd.hashtagsurfer.stream.instagram.TagService;
import com.ocd.hashtagsurfer.stream.instagram.TagSuggestionService;
import com.ocd.hashtagsurfer.util.Constants;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link StreamFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link StreamFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class StreamFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, ShutterbugManager.ShutterbugManagerListener {

    public static final String STREAM_RECEIVER = "STREAM_RECEIVER";

    public static final String MEDIA_KEY = "MEDIA_KEY";

    public static final int SPAN_COUNT = 3;

    public static final String TAG_KEY = "TAG_KEY";

    public static final String NEXT_PAGE_KEY = "NEXT_PAGE_KEY";

    public static final String LAT_KEY = "LAT_KEY";

    public static final String LON_KEY = "LON_KEY";

    public static final String SURF_TYPE_KEY = "SURF_TYPE_KEY";

    public static final long TWO_MINUTES = 2 * 60000L;

    public static final String INSTAGRAM_URL_KEYWORD = "instagram";

    public static final int SEARCH_THRESHOLD = 2;

    private static Location mLastLocation;

    private ProgressBar progressBar;

    private TextView empty;

    private String tag;

    private SurferActivity.SurfType type = SurferActivity.SurfType.INSTAGRAM_TAG;

    private OnFragmentInteractionListener mListener;

    private Object sync = new Object();

    private boolean lockSync = false;

    private int visibleThreshold = 20;

    private ProgressBar gridProgress;

    private MediaItem gridMedia;

    private String nextPage = null;

    private GoogleApiClient mGoogleApiClient;

    private SurferActivity.RefreshType actualRefresh = null;

    private StreamReceiver receiver;

    private GridLayoutManager mLayoutManager;

    private RecyclerView recyclerView;

    private SearchView searchView;

    public static final String[] columns = new String[] {BaseColumns._ID, SearchManager.SUGGEST_COLUMN_QUERY, SearchManager.SUGGEST_COLUMN_TEXT_2, Intent.ACTION_SEARCH};

    public StreamFragment() {
        super();
    }

    public StreamFragment(String tag, SurferActivity.SurfType type) {
        super();
        this.tag = tag;
        this.type = type;
        receiver = new StreamReceiver(new Handler());
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param tag Hashtag.
     * @return A new instance of fragment StreamFragment.
     */
    public static StreamFragment newInstance(String tag, SurferActivity.SurfType type) {
        StreamFragment fragment = new StreamFragment(tag, type);
        if (!SurferActivity.NEARBY.equals(tag)) {
            fragment.setHasOptionsMenu(true);
        }
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.i(StreamFragment.class.getSimpleName(), "Fragment.onCreateView");
        return inflater.inflate(R.layout.fragment_stream, container, false);
    }

    //    // TODO: Rename method, update argument and hook method into UI event
    //    public void onButtonPressed(Uri uri) {
    //        if (mListener != null) {
    //            mListener.onFragmentInteraction(uri);
    //        }
    //    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        recyclerView = (RecyclerView) view.findViewById(R.id.my_recycler_view);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        empty = (TextView) view.findViewById(R.id.empty);

        mLayoutManager = (GridLayoutManager) recyclerView.getLayoutManager();
        if (mLayoutManager == null) {
            mLayoutManager = new GridLayoutManager(getActivity(), SPAN_COUNT, LinearLayoutManager.VERTICAL, false);
        }
        mLayoutManager.setSmoothScrollbarEnabled(true);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(new StreamFragmentAdapter(StreamFragment.this));

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                if (mLayoutManager == null) {
                    return;
                }

                int visibleItemCount = mLayoutManager.getChildCount();
                int totalItemCount = mLayoutManager.getItemCount();
                int pastVisibleItems = mLayoutManager.findFirstVisibleItemPosition();
                if ((visibleItemCount + pastVisibleItems) >= totalItemCount - visibleThreshold) {
                    startSearch(SurferActivity.RefreshType.SCROLL);
                }
            }
        });

        MultiSwipeRefreshLayout swipeLayout = (MultiSwipeRefreshLayout) getView().findViewById(R.id.swipe_container);
        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                startSearch(SurferActivity.RefreshType.SWIPE);
            }
        });
        swipeLayout.setColorSchemeResources(R.color.primary, R.color.accent);
        swipeLayout.setSwipeableChildren(R.id.my_recycler_view);

        startSearch(SurferActivity.RefreshType.INIT);
    }

    @Override
    public void onDestroyView() {
        Log.i(StreamFragment.class.getSimpleName(), "Fragment.onDestroyView");
        if (mLayoutManager != null) {
            mLayoutManager.removeAllViews();
        }
        super.onDestroyView();
    }

    public void startSearch(SurferActivity.RefreshType refreshType) {
        actualRefresh = refreshType;
        Log.i(StreamFragment.class.getSimpleName(), "Search assertion locksync: " + lockSync + " refreshType: " + refreshType + " nextpage: " + nextPage + " tag: " + tag);
        switch (type) {
            case INSTAGRAM_TAG:
                startSearchForTag(refreshType);
                break;
            case INSTAGRAM_NEARBY:
                startSearchForNearby(refreshType);
                break;
        }
    }

    private void startSearchForTag(SurferActivity.RefreshType refreshType) {
        synchronized (sync) {
            if (lockSync) {
                return;
            }
            if (SurferActivity.RefreshType.SCROLL.equals(refreshType) && nextPage == null) {
                return;
            }

            lockSync = true;
            // Swipe has its own stuff for progress
            if (SurferActivity.RefreshType.INIT.equals(refreshType)) {
                progressBar.setVisibility(View.VISIBLE);
            }
            Intent search = new Intent(getActivity(), TagService.class);
            search.putExtra(SurferActivity.ACCESS_TOKEN_KEY_INSTAGRAM, SurferActivity.accessTokenInstagram);
            search.putExtra(STREAM_RECEIVER, receiver);
            if (nextPage != null) {
                search.putExtra(NEXT_PAGE_KEY, nextPage);
            } else {
                search.putExtra(TAG_KEY, tag);
            }
            search.putExtra(SurferActivity.REFRESH_TYPE_KEY, refreshType);
            getActivity().startService(search);
        }
    }

    private void startSearchForNearby(SurferActivity.RefreshType refreshType) {
        synchronized (sync) {
            if (lockSync) {
                return;
            }
            if (SurferActivity.RefreshType.SCROLL.equals(refreshType) && nextPage == null) {
                return;
            }

            lockSync = true;
            // Swipe has its own stuff for progress
            if (!SurferActivity.RefreshType.SWIPE.equals(refreshType)) {
                progressBar.setVisibility(View.VISIBLE);
            }

            if (mLastLocation != null && System.currentTimeMillis() - mLastLocation.getTime() < TWO_MINUTES) {
                Log.i(StreamFragment.class.getSimpleName(), "Have cached location!");
                haveLocation();
            } else {
                if (mGoogleApiClient != null && (mGoogleApiClient.isConnected() || mGoogleApiClient.isConnecting())) {
                    mGoogleApiClient.disconnect();
                }
                mGoogleApiClient = new GoogleApiClient.Builder(getActivity()).addConnectionCallbacks(this)
                                                                             .addOnConnectionFailedListener(this)
                                                                             .addApi(LocationServices.API)
                                                                             .build();
                mGoogleApiClient.connect();
            }
        }
    }

    @Override
    public void onConnected(Bundle connectionHint) {

        if (getActivity() == null) {
            return;
        }

        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (mLastLocation == null) {
            handleFailure();
            return;
        }

        haveLocation();

    }

    private void haveLocation() {

        //----------TEST
        //        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        //        Location mLastLocation = locationManager.getLastKnownLocation("gps");

        Intent search = new Intent(getActivity(), NearbyService.class);
        search.putExtra(SurferActivity.ACCESS_TOKEN_KEY_INSTAGRAM, SurferActivity.accessTokenInstagram);
        search.putExtra(STREAM_RECEIVER, receiver);
        search.putExtra(LAT_KEY, mLastLocation.getLatitude());
        search.putExtra(LON_KEY, mLastLocation.getLongitude());
        //        search.putExtra(LAT_KEY, 47.49791);
        //        search.putExtra(LON_KEY, 19.04023);
        search.putExtra(SurferActivity.REFRESH_TYPE_KEY, actualRefresh);
        getActivity().startService(search);

    }

    private void handleFailure() {
        lockSync = false;
        progressBar.setVisibility(View.GONE);
        mGoogleApiClient.disconnect();
        if (!isVisible() || isDetached() || getActivity() == null) {
            return;
        }
        animateText(R.string.no_gps, true);
    }

    @Override
    public void onConnectionSuspended(int i) {
        handleFailure();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        handleFailure();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (OnFragmentInteractionListener) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString() + " must implement OnFragmentInteractionListener");
        }

        // Get tracker.
        Tracker t = ((SurferApplication) getActivity().getApplication()).getTracker();

        // Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());
    }

    @Override
    public void onDetach() {
        mListener = null;
        super.onDetach();
    }

    public void startMediaDetails(ProgressBar progressBar, int position) {

        RecyclerView mRecyclerView = (RecyclerView) getView().findViewById(R.id.my_recycler_view);
        MediaItem item = ((StreamFragmentAdapter) mRecyclerView.getAdapter()).getItemAt(position);
        if (item.getType().equals(MediaItem.MEDIA_IMAGE_TYPE)) {
            downloadStandardRes(progressBar, item);
        } else if (item.getType().equals(MediaItem.MEDIA_VIDEO_TYPE)) {
            startMediaDetailsActivity(item);
        }

    }

    private void downloadStandardRes(final ProgressBar progressBar, final MediaItem mediaItem) {
        synchronized (sync) {
            if (lockSync) {
                return;
            }
            lockSync = true;
            progressBar.setVisibility(View.VISIBLE);
            final ShutterbugManager manager = ShutterbugManager.getSharedImageManager(getActivity());
            manager.cancel(this);
            if (gridProgress != null) {
                gridProgress.setVisibility(View.INVISIBLE);
            }
            this.gridMedia = mediaItem;
            this.gridProgress = progressBar;
            manager.download(mediaItem.getImages().getStandard_resolution().getUrl(), 2, 2, this);
        }
    }

    private void startMediaDetailsActivity(final MediaItem mediaItem) {
        final Intent intent = new Intent(getActivity(), MediaDetailsActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(TAG_KEY, tag);
        bundle.putSerializable(StreamFragment.SURF_TYPE_KEY, type);
        bundle.putSerializable(MEDIA_KEY, mediaItem);
        intent.putExtras(bundle);
        getActivity().startActivity(intent);
    }

    @Override
    public void onImageSuccess(Bitmap bitmap, String url) {
        lockSync = false;
        Log.i(StreamFragment.class.getSimpleName(), "Full resolution image fetched!");
        gridProgress.setVisibility(View.INVISIBLE);
        startMediaDetailsActivity(gridMedia);
    }

    @Override
    public void onImageFailure(String url) {
        lockSync = false;
        Log.e(StreamFragment.class.getSimpleName(), "Failed to fetch ull resolution image!");
        gridProgress.setVisibility(View.INVISIBLE);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    private void animateText(int resourceId, boolean visible) {
        final Animation anim;
        int visibility;
        if (visible) {
            if (empty.getVisibility() == View.VISIBLE) {
                return;
            }
            visibility = View.VISIBLE;
            anim = AnimationUtils.loadAnimation(getActivity(), android.R.anim.fade_in);
        } else {
            if (empty.getVisibility() == View.INVISIBLE) {
                return;
            }
            visibility = View.INVISIBLE;
            anim = AnimationUtils.loadAnimation(getActivity(), android.R.anim.fade_out);
        }

        anim.setDuration(Constants.DURATION_MILLIS);
        anim.setFillAfter(true);
        if (resourceId != 0) {
            empty.setText(getString(resourceId));
        }
        empty.setAnimation(anim);
        empty.setVisibility(visibility);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.search) {
            return true;
        } else if (id == R.id.import_export) {
            if (getActivity() != null) {
                // Get tracker.
                Tracker t = ((SurferApplication) getActivity().getApplication()).getTracker();
                // Send a screen view.
                t.send(new HitBuilders.EventBuilder().setCategory(MediaDetailsActivity.class.getSimpleName())
                                                     .setAction("ImportExport")
                                                     .setLabel("Import/Export")
                                                     .build());
                ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
                if (clipboard.hasPrimaryClip()) {
                    ClipData.Item clipItem = clipboard.getPrimaryClip().getItemAt(0);
                    String instaURL = clipItem.getText().toString();
                    if (instaURL != null && instaURL.contains(INSTAGRAM_URL_KEYWORD)) {
                        String[] split = instaURL.split("/");
                        startMediaSearch(split[split.length - 1]);
                    } else {
                        copyPasteError();
                    }
                } else {
                    copyPasteError();
                }
            } else {
                copyPasteError();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    private void copyPasteError() {
        if (getActivity() != null) {
            Toast.makeText(getActivity(), getActivity().getString(R.string.not_and_insta_url), Toast.LENGTH_LONG)
                 .show();
        }
    }

    private void downloadError() {
        if (getActivity() != null) {
            Toast.makeText(getActivity(), getActivity().getString(R.string.save_failed), Toast.LENGTH_LONG)
                 .show();
        }
    }

    private void startMediaSearch(String mediaKey) {
        synchronized (sync) {
            if (lockSync) {
                return;
            }
            lockSync = true;
            progressBar.setVisibility(View.VISIBLE);
            Intent search = new Intent(getActivity(), MediaService.class);
            search.putExtra(SurferActivity.ACCESS_TOKEN_KEY_INSTAGRAM, SurferActivity.accessTokenInstagram);
            search.putExtra(StreamFragment.MEDIA_KEY, mediaKey);
            search.putExtra(STREAM_RECEIVER, receiver);
            getActivity().startService(search);
        }
    }

    private void startTagSuggestionSearch(String tagKey) {
        Intent search = new Intent(getActivity(), TagSuggestionService.class);
        search.putExtra(SurferActivity.ACCESS_TOKEN_KEY_INSTAGRAM, SurferActivity.accessTokenInstagram);
        search.putExtra(StreamFragment.TAG_KEY, tagKey);
        search.putExtra(STREAM_RECEIVER, receiver);
        getActivity().startService(search);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        if (!SurferActivity.NEARBY.equals(tag)) {
            menu.clear();
            // Inflate the menu; this adds items to the action bar if it is present.
            getActivity().getMenuInflater().inflate(R.menu.menu_surfer, menu);
            // Associate searchable configuration with the SearchView
            SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
            searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.search));
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    searchView.onActionViewCollapsed();
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    return true;
                }

            });

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                int id = searchView.getContext()
                                   .getResources()
                                   .getIdentifier("android:id/search_src_text", null, null);
                TextView textView = (TextView) searchView.findViewById(id);
                if (textView != null) {
                    textView.setHintTextColor(getResources().getColor(R.color.primary_hint));
                }

                id = searchView.getContext()
                               .getResources()
                               .getIdentifier("android:id/search_button", null, null);
                ImageView searchButton = (ImageView) searchView.findViewById(id);
                if (searchButton != null) {
                    searchButton.setImageResource(R.drawable.ic_ab_search);
                }

                id = searchView.getContext()
                               .getResources()
                               .getIdentifier("android:id/search_voice_btn", null, null);
                ImageView voiceButton = (ImageView) searchView.findViewById(id);
                if (voiceButton != null) {
                    voiceButton.setImageResource(R.drawable.ic_ab_voice);
                }

                id = searchView.getContext()
                               .getResources()
                               .getIdentifier("android:id/search_mag_icon", null, null);
                ImageView hintIcon = (ImageView) searchView.findViewById(id);
                if (hintIcon != null) {
                    hintIcon.setVisibility(View.GONE);
                }

                id = searchView.getContext()
                               .getResources()
                               .getIdentifier("android:id/search_close_btn", null, null);
                ImageView closeButton = (ImageView) searchView.findViewById(id);
                if (closeButton != null) {
                    closeButton.setImageResource(R.drawable.ic_ab_cancel);
                }
            }

            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    if (newText.length() < SEARCH_THRESHOLD) {
                        if (searchView.getSuggestionsAdapter() != null && searchView.getSuggestionsAdapter()
                                                                                    .getCount() > 0) {
                            searchView.getSuggestionsAdapter()
                                      .swapCursor(new MatrixCursor(columns, 0));
                        }
                        return false;
                    }

                    startTagSuggestionSearch(newText);

                    return true;
                }
            });
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        } else {
            super.onPrepareOptionsMenu(menu);
        }

    }

    private class StreamReceiver extends ResultReceiver {

        /**
         * Create a new ResultReceive to receive results.  Your
         * {@link #onReceiveResult} method will be called from the thread running
         * <var>handler</var> if given, or from an arbitrary thread if null.
         *
         * @param handler thread communication support.
         */
        public StreamReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            lockSync = false;
            if (getActivity() == null) {
                return;
            }
            MultiSwipeRefreshLayout swipeLayout = (MultiSwipeRefreshLayout) getView().findViewById(R.id.swipe_container);
            swipeLayout.setRefreshing(false);
            if (resultData.getSerializable(TagService.SEARCH_RESULT) instanceof SearchResponse) {
                progressBar.setVisibility(View.GONE);
                SearchResponse response = (SearchResponse) resultData.getSerializable(TagService.SEARCH_RESULT);
                if (resultCode == Constants.RESULT_CODE_OK) {
                    SurferActivity.RefreshType refreshType = (SurferActivity.RefreshType) resultData
                            .getSerializable(SurferActivity.REFRESH_TYPE_KEY);
                    if (response != null && response.getData().size() > 0) {
                        animateText(0, false);
                        nextPage = resultData.getString(NEXT_PAGE_KEY);
                        if (recyclerView == null) {
                            return;
                        }
                        StreamFragmentAdapter adapter = (StreamFragmentAdapter) recyclerView.getAdapter();
                        if (SurferActivity.RefreshType.SWIPE.equals(refreshType)) {
                            adapter.setItems(response.getData());
                        } else {
                            adapter.appendItems(response.getData());
                        }
                    } else {
                        animateText(R.string.no_hits_for_this_tag, true);
                    }
                } else if (resultCode == Constants.RESULT_CODE_NETWORK_ERROR) {
                    animateText(R.string.no_internet, true);
                } else if (resultCode == Constants.RESULT_CODE_GPS_ERROR) {
                    animateText(R.string.no_gps, true);
                } else if (resultCode == Constants.RESULT_CODE_ACCESS_TOKEN_EXPIRED) {
                    animateText(R.string.access_token_expired, true);
                    progressBar.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            getActivity().startActivityForResult(new Intent(getActivity(), InstaAuthorizeActivity.class), 1);
                        }
                    }, 2000);
                }
            } else if (resultData.getSerializable(TagService.SEARCH_RESULT) instanceof MediaSearchResponse) {
                MediaSearchResponse response = (MediaSearchResponse) resultData.getSerializable(TagService.SEARCH_RESULT);
                if (response != null) {
                    if (getActivity() != null) {
                        // Get tracker.
                        Tracker t = ((SurferApplication) getActivity().getApplication()).getTracker();
                        // Send a screen view.
                        t.send(new HitBuilders.EventBuilder().setCategory(StreamFragment.class.getSimpleName())
                                                             .setAction("Import")
                                                             .setLabel("Import")
                                                             .build());
                    }
                    if (MediaItem.MEDIA_IMAGE_TYPE.equals(response.getData().getType())) {
                        downloadStandardRes(progressBar, response.getData());
                    } else {
                        startMediaDetailsActivity(response.getData());
                    }
                } else {
                    downloadError();
                }
            } else if (resultData.getSerializable(TagService.SEARCH_RESULT) instanceof TagSuggestionResponse) {
                final TagSuggestionResponse response = (TagSuggestionResponse) resultData.getSerializable(TagService.SEARCH_RESULT);
                final MatrixCursor cursor = new MatrixCursor(columns, response.getData().size());
                List<TagSuggestion> suggestions = response.getData();
                for (TagSuggestion suggestion : suggestions) {
                    cursor.addRow(new Object[] {suggestion.hashCode(), suggestion.getName(), suggestion
                            .getMedia_count(), SearchManager.QUERY});
                }
                CursorAdapter adapter = searchView.getSuggestionsAdapter();
                if (adapter == null) {
                    searchView.setSuggestionsAdapter(new SuggestionAdapter(getActivity(), cursor, false));
                } else {
                    adapter.swapCursor(cursor);
                }
            }
            super.onReceiveResult(resultCode, resultData);
        }

    }

    @Override
    public String toString() {
        return "StreamFragment{" +
                "tag='" + tag + '\'' +
                ", type=" + type +
                ", nextPage='" + nextPage + '\'' +
                '}' + hashCode();
    }
}
