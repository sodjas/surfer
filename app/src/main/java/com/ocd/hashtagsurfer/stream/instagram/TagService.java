package com.ocd.hashtagsurfer.stream.instagram;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import com.ocd.hashtagsurfer.model.instagram.SearchResponse;
import com.ocd.hashtagsurfer.stream.StreamFragment;
import com.ocd.hashtagsurfer.stream.SurferActivity;
import com.ocd.hashtagsurfer.util.Constants;
import com.ocd.hashtagsurfer.util.Parser;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.nio.charset.Charset;


/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 */
public class TagService extends AbstractInstagramService {

    private static final String SEARCH_POPULAR = "https://api.instagram.com/v1/media/popular?access_token=";
    private static final String SEARCH_FOR_TAG = "https://api.instagram.com/v1/tags/";
    private static final String TOKEN_SUFFIX = "/media/recent?access_token=";
    private static final String LOG_TAG = TagService.class.getSimpleName();

    public TagService() {
        super(LOG_TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        if (intent == null || (!intent.hasExtra(StreamFragment.TAG_KEY)
                && !intent.hasExtra(StreamFragment.NEXT_PAGE_KEY))) {
            throw new IllegalArgumentException("Mandatory params missing!");
        }

        if (!intent.hasExtra(SurferActivity.REFRESH_TYPE_KEY)
                || !intent.hasExtra(SurferActivity.ACCESS_TOKEN_KEY_INSTAGRAM)
                || !intent.hasExtra(StreamFragment.STREAM_RECEIVER)) {
            throw new IllegalArgumentException("Mandatory params missing!");
        }
        SearchResponse firstResponse = null;
        //SearchResponse secondResponse = null;
        int resultCode = Constants.RESULT_CODE_OK;
        HttpURLConnection urlConnection = null;
        try {
            String hashTag = null;
            if (intent.hasExtra(StreamFragment.TAG_KEY)) {
                hashTag = intent.getStringExtra(StreamFragment.TAG_KEY);
                if (hashTag.equals(SurferActivity.POPULAR)) {
                    urlConnection = connectToURL(SEARCH_POPULAR
                            + intent.getStringExtra(SurferActivity.ACCESS_TOKEN_KEY_INSTAGRAM));
                } else {
                    urlConnection = connectToURL(SEARCH_FOR_TAG
                            + URLEncoder.encode(hashTag, Charset.defaultCharset().displayName())
                            + TOKEN_SUFFIX
                            + intent.getStringExtra(SurferActivity.ACCESS_TOKEN_KEY_INSTAGRAM));
                }
            } else if (intent.hasExtra(StreamFragment.NEXT_PAGE_KEY)) {
                urlConnection = connectToURL(intent.getStringExtra(StreamFragment.NEXT_PAGE_KEY));
            }

            int status = urlConnection.getResponseCode();
            Log.i(LOG_TAG, "Tag search returned with status: " + status);
            switch (status) {
                case HttpURLConnection.HTTP_OK:
                    // Parse response
                    firstResponse = Parser.extractSearchResults(readStream(urlConnection.getInputStream()));
                    urlConnection.disconnect();

//                    if (hashTag != null  && hashTag.equals(StreamActivity.POPULAR)
//                            || firstResponse.getPagination() == null
//                            || firstResponse.getPagination().getNext_url() == null) {
//                        secondResponse = firstResponse;
//                        break;
//                    }
//                    // Get next page in one round
//                    urlConnection = connectToURL(firstResponse.getPagination().getNext_url());
//                    status = urlConnection.getResponseCode();
//                    switch (status) {
//                        case HttpURLConnection.HTTP_OK:
//                            // Parse response
//                            secondResponse = Parser.extractSearchResults(readStream(urlConnection.getInputStream()));
//                            secondResponse.getData().addAll(firstResponse.getData());
//                            urlConnection.disconnect();
//                            break;
//                        default:
//                            Log.e(LOG_TAG, "Problem is: " + urlConnection.getResponseMessage());
//                            if (checkIfExpired(urlConnection)) {
//                                resultCode = Constants.RESULT_CODE_ACCESS_TOKEN_EXPIRED;
//                            }
//                            break;
//                    }
                    break;
                default:
                    Log.e(LOG_TAG, "Problem is: " + urlConnection.getResponseMessage() + " ");
                    if (checkIfExpired(urlConnection)) {
                        resultCode = Constants.RESULT_CODE_ACCESS_TOKEN_EXPIRED;
                    }
                    break;
            }

        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Bad URL", e);
            resultCode = Constants.RESULT_CODE_PROTOCOL_ERROR;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Failed to talk with Instagram", e);
            resultCode = Constants.RESULT_CODE_NETWORK_ERROR;
        } finally {
            ResultReceiver receiver = intent.getParcelableExtra(StreamFragment.STREAM_RECEIVER);
            if (receiver == null) {
                return;
            }
            Bundle result = new Bundle();
            result.putSerializable(SEARCH_RESULT, firstResponse);
            if (firstResponse!= null && firstResponse.getPagination() != null) {
                result.putString(StreamFragment.NEXT_PAGE_KEY, firstResponse.getPagination().getNext_url());
            }
            result.putSerializable(SurferActivity.REFRESH_TYPE_KEY, intent.getSerializableExtra(SurferActivity.REFRESH_TYPE_KEY));
            receiver.send(resultCode, result);
        }

    }

}
