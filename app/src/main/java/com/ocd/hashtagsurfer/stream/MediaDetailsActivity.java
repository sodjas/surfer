package com.ocd.hashtagsurfer.stream;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.applidium.shutterbug.FetchableImageView;
import com.applidium.shutterbug.utils.ShutterbugManager;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.ocd.hashtagsurfer.R;
import com.ocd.hashtagsurfer.app.SurferApplication;
import com.ocd.hashtagsurfer.model.instagram.MediaItem;
import com.ocd.hashtagsurfer.model.instagram.SearchResponse;
import com.ocd.hashtagsurfer.stream.instagram.FetchService;
import com.ocd.hashtagsurfer.stream.instagram.TagService;
import com.ocd.hashtagsurfer.util.Constants;
import com.ocd.hashtagsurfer.util.DependencyUtil;
import com.ocd.hashtagsurfer.util.StorageUtil;

import org.apache.commons.io.FileUtils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *
 */
public class MediaDetailsActivity extends AppCompatActivity {

    public static final String FETCH_RECEIVER = "FETCH_RECEIVER";

    public static final String SAVE_TYPE_KEY = "SAVE_TYPE_KEY";

    public static final String MOVIE_URL_KEY = "MOVIE_URL_KEY";

    private static final String LOG_TAG = MediaDetailsActivity.class.getSimpleName();

    private static final String COM_INSTAGRAM_ANDROID = "com.instagram.android";

    private static final String USER_URL_FALLBACK = "http://instagram.com/";

    private static final String USER_URL = USER_URL_FALLBACK + "_u/";

    private MediaItem media;

    private String tag;

    private String url;

    private FetchableImageView profileImage = null;

    private ImageView imageView = null;

    private ProgressBar progressBar;

    private TextView username;

    private TextView caption;

    private TextView likes;

    private TextView map;

    private boolean lockSync = false;

    private enum SaveType {
        SAVE,

        SHARE
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_media_details);
        Bundle bundle = getIntent().getExtras();
        media = (MediaItem) bundle.getSerializable(StreamFragment.MEDIA_KEY);
        tag = (String) bundle.get(StreamFragment.TAG_KEY);
        url = media.getImages().getStandard_resolution().getUrl();
        profileImage = (FetchableImageView) findViewById(R.id.profile_image);
        imageView = (ImageView) findViewById(R.id.imageView);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        username = (TextView) findViewById(R.id.username);
        caption = (TextView) findViewById(R.id.caption);
        likes = (TextView) findViewById(R.id.like_count);

        if (media.getLocation() == null) {
            findViewById(R.id.map).setVisibility(View.GONE);
        } else {
            map = (TextView) findViewById(R.id.map);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        final ViewTreeObserver viewTreeObserver = profileImage.getViewTreeObserver();
        if (viewTreeObserver.isAlive()) {
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    final ViewTreeObserver viewTreeObserver = profileImage.getViewTreeObserver();
                    if (viewTreeObserver.isAlive()) {
                        viewTreeObserver.removeOnGlobalLayoutListener(this);
                        profileImage.setImage(media.getUser()
                                                   .getProfile_picture(), profileImage.getWidth(), profileImage
                                .getHeight());
                    }
                }
            });
        }

        username.setText(media.getUser().getUsername());

        if (map != null) {
            map.setText(media.getLocation().getName());
        }

        if (media.getLikes() != null) {
            likes.setText(Integer.toString(media.getLikes().getCount()));
        } else {
            likes.setText(Integer.toString(0));
        }
        if (media.getCaption() != null) {
            caption.setText(media.getCaption().getText());
        }

        if (media.getType().equals(MediaItem.MEDIA_IMAGE_TYPE)) {

            imageView.setVisibility(View.VISIBLE);
            File cachedFile = new File(getCacheDir(), ShutterbugManager.getSharedImageManager(this)
                                                                       .getCacheFilePathForOneValueCount(url));
            imageView.setImageURI(Uri.fromFile(cachedFile));
            imageView.setAdjustViewBounds(true);

        } else if (media.getType().equals(MediaItem.MEDIA_VIDEO_TYPE)) {

            final VideoView videoView = (VideoView) findViewById(R.id.videoView);
            final MediaController controller = new MediaController(this);
            videoView.setMediaController(controller);
            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    imageView.setVisibility(View.GONE);
                    progressBar.setVisibility(View.GONE);
                    mp.setVideoScalingMode(MediaPlayer.VIDEO_SCALING_MODE_SCALE_TO_FIT);
                    mp.setVolume(0f, 0f);
                    controller.setAnchorView(videoView);
                    controller.show(2000);
                    videoView.start();
                }
            });
            progressBar.setVisibility(View.VISIBLE);
            videoView.setVisibility(View.VISIBLE);
            videoView.setMinimumWidth(media.getVideos().getStandard_resolution().getWidth());
            videoView.setMinimumHeight(media.getVideos().getStandard_resolution().getHeight());
            videoView.setVideoURI(Uri.parse(media.getVideos().getStandard_resolution().getUrl()));

        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Get tracker.
        Tracker t = ((SurferApplication) getApplication()).getTracker();

        // Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());
    }

    public void shareClick(View view) {

        // Get tracker.
        Tracker t = ((SurferApplication) getApplication()).getTracker();

        if (!StorageUtil.isExternalStorageWritable()) {
            Toast.makeText(this, getString(R.string.check_your_storage), Toast.LENGTH_LONG).show();
            return;
        }
        if (MediaItem.MEDIA_IMAGE_TYPE.equals(media.getType())) {
            shareImage();
            // Send a screen view.
            t.send(new HitBuilders.EventBuilder().setCategory(MediaDetailsActivity.class.getSimpleName())
                                                 .setAction("ShareImage")
                                                 .setLabel("ShareImage")
                                                 .build());
        } else if (MediaItem.MEDIA_VIDEO_TYPE.equals(media.getType()) && !lockSync) {
            lockSync = true;
            saveVideo(SaveType.SHARE);
            // Send a screen view.
            t.send(new HitBuilders.EventBuilder().setCategory(MediaDetailsActivity.class.getSimpleName())
                                                 .setAction("ShareVideo")
                                                 .setLabel("ShareVideo")
                                                 .build());

        }
    }

    public void saveClick(View view) {

        // Get tracker.
        Tracker t = ((SurferApplication) getApplication()).getTracker();

        if (!StorageUtil.isExternalStorageWritable()) {
            Toast.makeText(this, getString(R.string.check_your_storage), Toast.LENGTH_LONG).show();
            return;
        }
        if (MediaItem.MEDIA_IMAGE_TYPE.equals(media.getType())) {
            saveImage();
            // Send a screen view.
            t.send(new HitBuilders.EventBuilder().setCategory(MediaDetailsActivity.class.getSimpleName())
                                                 .setAction("SaveImage")
                                                 .setLabel("SaveImage")
                                                 .build());
        } else if (MediaItem.MEDIA_VIDEO_TYPE.equals(media.getType()) && !lockSync) {
            lockSync = true;
            saveVideo(SaveType.SAVE);
            // Send a screen view.
            t.send(new HitBuilders.EventBuilder().setCategory(MediaDetailsActivity.class.getSimpleName())
                                                 .setAction("SaveVideo")
                                                 .setLabel("SaveVideo")
                                                 .build());
        }
    }

    private void shareImage() {
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("image/*");
        long date = 0;
        String username = "a";
        String caption = "";
        if (media.getUser() != null) {
            username = media.getUser().getUsername();
        }
        if (media.getCaption() != null) {
            caption = media.getCaption().getText();
            date = media.getCaption().getCreationTime();
        }
        String text = getString(R.string.repost_from) + username + "\n" + caption + "\n" + "via @hashtagsurfer";
        share.putExtra(Intent.EXTRA_TEXT, text);
        try {
            File cachedFile = new File(getCacheDir(), ShutterbugManager.getSharedImageManager(this)
                                                                       .getCacheFilePathForOneValueCount(url));
            File dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/" + tag);
            dir.mkdirs();
            File shareCopy = new File(dir, username + "-" + date + ".jpg");
            if (!shareCopy.exists()) {
                shareCopy.createNewFile();
            }
            FileUtils.copyFile(cachedFile, new BufferedOutputStream(new FileOutputStream(shareCopy)));
            share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(shareCopy));
            startActivity(Intent.createChooser(share, text));
        } catch (ActivityNotFoundException e) {
            Log.e(LOG_TAG, "Failed to prepare share media", e);
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
        } catch (IOException e) {
            Log.e(LOG_TAG, "Failed to prepare share media", e);
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
        }
    }

    private void saveImage() {
        try {
            long date = 0;
            String username = "a";
            if (media.getUser() != null) {
                username = media.getUser().getUsername();
            }
            if (media.getCaption() != null) {
                date = media.getCaption().getCreationTime();
            }

            File cachedFile = new File(getCacheDir(), ShutterbugManager.getSharedImageManager(this)
                                                                       .getCacheFilePathForOneValueCount(url));
            File pictures = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            File hashTag = new File(pictures, tag);
            if (!hashTag.mkdirs() && !hashTag.exists()) {
                Log.e(LOG_TAG, "Failed to create dir for tag(" + tag + "): " + hashTag.getAbsolutePath());
            }
            File copy = new File(hashTag, username + "-" + date + ".jpg");
            if (!copy.exists()) {
                copy.createNewFile();
            }
            FileUtils.copyFile(cachedFile, new BufferedOutputStream(new FileOutputStream(copy)));
            Toast.makeText(this, getString(R.string.save_success), Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            Log.e(MediaDetailsActivity.class.getSimpleName(), "Failed to save file", e);
            Toast.makeText(this, getString(R.string.save_failed), Toast.LENGTH_LONG).show();
        }
    }

    private void shareVideo(String file) {
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("video/*");
        String username = "a";
        String caption = "";
        if (media.getUser() != null) {
            username = media.getUser().getUsername();
        }
        if (media.getCaption() != null) {
            caption = media.getCaption().getText();
        }

        String text = getString(R.string.repost_from) + username + "\n" + caption + "\n" + "via @hashtagsurfer";
        share.putExtra(Intent.EXTRA_TEXT, text);
        try {
            Log.i(LOG_TAG, "Sharing video: " + file);
            File shareCopy = new File(file);
            share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(shareCopy));
            startActivity(Intent.createChooser(share, text));
        } catch (ActivityNotFoundException e) {
            Log.e(LOG_TAG, "Failed to prepare share media", e);
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
        }
    }

    private void saveVideo(SaveType type) {
        progressBar.setVisibility(View.VISIBLE);
        Intent save = new Intent(this, FetchService.class);
        save.putExtra(SAVE_TYPE_KEY, type);
        save.putExtra(MOVIE_URL_KEY, media.getVideos().getStandard_resolution().getUrl());
        save.putExtra(StreamFragment.TAG_KEY, tag);
        save.putExtra(FETCH_RECEIVER, new FetchReceiver(new Handler()));
        startService(save);
    }

    public void openInAppClick(View view) {
        String link = media.getLink();
        openLink(link);
    }

    public void goToUserClick(View view) {
        // Get tracker.
        Tracker t = ((SurferApplication) getApplication()).getTracker();
        // Send a screen view.
        t.send(new HitBuilders.EventBuilder().setCategory(MediaDetailsActivity.class.getSimpleName())
                                             .setAction("GoToUser")
                                             .setLabel("Go to User")
                                             .build());

        String username = "a";
        if (media.getUser() != null) {
            username = media.getUser().getUsername();
        }
        openLink(USER_URL + username);
    }

    private void openLink(String link) {

        // Get tracker.
        Tracker t = ((SurferApplication) getApplication()).getTracker();
        // Send a screen view.
        t.send(new HitBuilders.EventBuilder().setCategory(MediaDetailsActivity.class.getSimpleName())
                                             .setAction("OpenLink")
                                             .setLabel("Open Link")
                                             .build());

        Uri uri = Uri.parse(link);
        Intent open = new Intent(Intent.ACTION_VIEW, uri);
        open.setPackage(COM_INSTAGRAM_ANDROID);
        try {
            startActivity(open);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(link)));
        }
    }

    /**
     *
     */
    public void navigateWithNativeMaps(View view) {

        // Get tracker.
        Tracker t = ((SurferApplication) getApplication()).getTracker();
        // Send a screen view.
        t.send(new HitBuilders.EventBuilder().setCategory(MediaDetailsActivity.class.getSimpleName())
                                             .setAction("Navigate")
                                             .setLabel("Navigate")
                                             .build());

        if (DependencyUtil.isGoogleMapsInstalled(this)) {
            final String testUri = "http://maps.google.com/maps?daddr=" + media.getLocation()
                                                                               .getLatitude() + "," + media
                    .getLocation()
                    .getLongitude();
            final Intent maps = new Intent(Intent.ACTION_VIEW, Uri.parse(testUri));
            maps.setPackage(DependencyUtil.COM_GOOGLE_ANDROID_APPS_MAPS);
            startActivity(maps);
        } else {
            DependencyUtil.startGoogleMapsListener(this);
        }
    }

    private class FetchReceiver extends ResultReceiver {

        /**
         * Create a new ResultReceive to receive results.  Your
         * {@link #onReceiveResult} method will be called from the thread running
         * <var>handler</var> if given, or from an arbitrary thread if null.
         *
         * @param handler thread communication support.
         */
        public FetchReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            progressBar.setVisibility(View.GONE);
            SearchResponse response = (SearchResponse) resultData.getSerializable(TagService.SEARCH_RESULT);
            if (resultCode == Constants.RESULT_CODE_OK) {
                lockSync = false;
                SaveType type = (SaveType) resultData.getSerializable(SAVE_TYPE_KEY);
                switch (type) {
                    case SAVE:
                        Toast.makeText(MediaDetailsActivity.this, getString(R.string.save_video_success), Toast.LENGTH_LONG)
                             .show();
                        sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment
                                .getExternalStorageDirectory())));
                        break;
                    case SHARE:
                        shareVideo(resultData.getString(FetchService.MOVIE_ON_SD));
                        break;
                }
            } else if (resultCode == Constants.RESULT_CODE_NETWORK_ERROR) {
                lockSync = false;

                Log.e(MediaDetailsActivity.class.getSimpleName(), "Failed to save video");
                Toast.makeText(MediaDetailsActivity.this, getString(R.string.save_failed), Toast.LENGTH_LONG)
                     .show();
            }
            super.onReceiveResult(resultCode, resultData);
        }

    }
}
