package com.ocd.hashtagsurfer.stream;

import android.app.SearchManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.ocd.hashtagsurfer.R;
import com.ocd.hashtagsurfer.app.SurferApplication;
import com.ocd.hashtagsurfer.component.UnderlinePageIndicator;
import com.ocd.hashtagsurfer.feedback.FeedbackHandler;
import com.ocd.hashtagsurfer.feedback.FeedbackStates;
import com.ocd.hashtagsurfer.model.instagram.UserInfo;
import com.ocd.hashtagsurfer.stream.instagram.TagService;
import com.ocd.hashtagsurfer.stream.instagram.UserInfoService;
import com.revmob.RevMob;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class SurferActivity extends AppCompatActivity implements StreamFragment.OnFragmentInteractionListener {

    public static final long ONE_WEEK_IN_MILLIS = 1000 * 60 * 60 * 24 * 7;
    public static final String ACCESS_TOKEN_KEY_INSTAGRAM = "ACCESS_TOKEN_KEY_INSTAGRAM";
    public static final String REFRESH_TYPE_KEY = "REFRESH_TYPE_KEY";
    public static final String POPULAR = "popular";
    public static final String NEARBY = "nearby";

    public static final String PROFILE_RECEIVER = "PROFILE_RECEIVER";

    public static String accessTokenInstagram;
    public String query = POPULAR;
    private boolean search = true;
    private RevMob revmob;

    private Toolbar toolbar;
    private ProfileReceiver receiver;
    private UserInfo info;

    @Override
    public void onFragmentInteraction(Uri uri) {
    }

    public void handleFeedbackAnswer(FeedbackStates mainYes) {
        new FeedbackHandler(this, R.id.main_fragmentcontainer).handleFeedback(mainYes);
    }

    public enum RefreshType {
        INIT,

        SWIPE,

        SCROLL;
    }

    public enum SurfType {
        INSTAGRAM_TAG,

        INSTAGRAM_NEARBY;
    }

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;

    UnderlinePageIndicator pagerIndicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_surfer);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        pagerIndicator = (UnderlinePageIndicator) findViewById(R.id.pagerIndicator);
        pagerIndicator.setActivity(this);

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        accessTokenInstagram = pref.getString(ACCESS_TOKEN_KEY_INSTAGRAM, "");
        if (getIntent() != null) {
            handleIntent(getIntent());
        }

        new FeedbackHandler(this, R.id.main_fragmentcontainer).handleFeedback(FeedbackStates.MAIN_START);

        cacheClear();
        Log.i(SurferActivity.class.getSimpleName(), "Oncreate");

        receiver = new ProfileReceiver(new Handler());

    }

    @Override
    protected void onStart() {
        super.onStart();
        // Get tracker.
        Tracker t = ((SurferApplication) getApplication()).getTracker();

        // Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());
    }

    private void cacheClear() {
        File dir = getCacheDir();
        File[] files = dir.listFiles();
        long oneWeek = System.currentTimeMillis() - ONE_WEEK_IN_MILLIS;
        for (File file : files) {
            if (FileUtils.isFileOlder(file, oneWeek)) {
                file.delete();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (info == null) {
            final Intent userInfo = new Intent(this, UserInfoService.class);
            userInfo.putExtra(ACCESS_TOKEN_KEY_INSTAGRAM, accessTokenInstagram);
            userInfo.putExtra(PROFILE_RECEIVER, receiver);
            startService(userInfo);
        }

        if (search) {
            // Create the adapter that will return a fragment for each of the three
            // primary sections of the activity.
            mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

            // Set up the ViewPager with the sections adapter.
            mViewPager = (ViewPager) findViewById(R.id.pager);
            mViewPager.setAdapter(mSectionsPagerAdapter);
            pagerIndicator.setViewPager(mViewPager, 0);
            toolbar.setTitle(query);
            search = false;
        }
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    /**
     * Assuming this activity was started with a new intent, process the incoming information and
     * react accordingly.
     *
     * @param intent
     */
    private void handleIntent(Intent intent) {
        // Special processing of the incoming intent only occurs if the if the action specified
        // by the intent is ACTION_SEARCH.
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            Log.i(SurferActivity.class.getSimpleName(), "Search hasQuery" + intent.hasExtra(SearchManager.QUERY) + " hasSuggest" + intent
                    .hasExtra(SearchManager.SUGGEST_COLUMN_QUERY));
            if (intent.hasExtra(SearchManager.QUERY)) {
                query = intent.getStringExtra(SearchManager.QUERY);
                search = true;
            } else if (intent.hasExtra(SearchManager.SUGGEST_COLUMN_QUERY)) {
                query = intent.getStringExtra(SearchManager.SUGGEST_COLUMN_QUERY);
                search = true;
            }
            // Get tracker.
            Tracker t = ((SurferApplication) getApplication()).getTracker();
            // Send a screen view.
            t.send(new HitBuilders.EventBuilder().setCategory(MediaDetailsActivity.class.getSimpleName())
                                                 .setAction("Search")
                                                 .setLabel("Search " + query)
                                                 .build());
        }
    }

    /**
     * @param view instance that is clicked in iconbar.
     */
    public void barClicked(final View view) {
        switch (view.getId()) {
            case R.id.popular:
                mViewPager.setCurrentItem(0);
                break;
            case R.id.nearby:
                mViewPager.setCurrentItem(1);
                break;
            default:
                break;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

        List<StreamFragment> items = new ArrayList<>();

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
            items.add(StreamFragment.newInstance(query, SurfType.INSTAGRAM_TAG));
            items.add(StreamFragment.newInstance(NEARBY, SurfType.INSTAGRAM_NEARBY));
        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return query;
                case 1:
                    return NEARBY;
            }
            return null;
        }

        @Override
        public android.support.v4.app.Fragment getItem(int i) {
            return items.get(i);
        }

    }

    private class ProfileReceiver extends ResultReceiver {

        /**
         * Create a new ResultReceive to receive results.  Your
         * {@link #onReceiveResult} method will be called from the thread running
         * <var>handler</var> if given, or from an arbitrary thread if null.
         *
         * @param handler
         */
        public ProfileReceiver(final Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(final int resultCode, final Bundle resultData) {
            super.onReceiveResult(resultCode, resultData);
            UserInfo resultInfo = (UserInfo) resultData.getSerializable(TagService.SEARCH_RESULT);
            if (resultInfo != null) {
                SurferActivity.this.info = resultInfo;
                //                final FetchableImageView imageView = new FetchableImageView(SurferActivity.this, null);
                //                imageView.setListener(new FetchableImageView.FetchableImageViewListener() {
                //                    @Override
                //                    public void onImageFetched(final Bitmap bitmap, final String url) {
                //                        toolbar.setNavigationIcon(new BitmapDrawable(getResources(), bitmap));
                //                    }
                //
                //                    @Override
                //                    public void onImageFailure(final String url) {
                //
                //                    }
                //                });
                //                imageView.setImage(info.getData().getProfile_picture(), 0, 0);
            }

        }
    }

}
