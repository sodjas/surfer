package com.ocd.hashtagsurfer.stream;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ocd.hashtagsurfer.R;

/**
 * Created by rakz on 22/02/15 Copyright OCDTeam.
 */
public class SuggestionAdapter extends CursorAdapter {

    public SuggestionAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {

        View view = LayoutInflater.from(context).inflate(R.layout.suggestion, null);

        SuggestionHolder holder = new SuggestionHolder();

        holder.hashTag = (TextView) view.findViewById(R.id.tagSuggestion);
        holder.count = (TextView) view.findViewById(R.id.tagSuggestionCount);

        view.setTag(holder);

        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        SuggestionHolder holder = (SuggestionHolder) view.getTag();
        int hashtagIndex = cursor.getColumnIndex(StreamFragment.columns[1]);
        int countIndex = cursor.getColumnIndex(StreamFragment.columns[2]);
        holder.hashTag.setText("#" + cursor.getString(hashtagIndex));
        holder.count.setText(Integer.toString(cursor.getInt(countIndex)));
    }

    private static class SuggestionHolder {
        TextView hashTag;
        TextView count;
    }
}
