package com.ocd.hashtagsurfer.stream.instagram;

import android.app.IntentService;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.ocd.hashtagsurfer.model.instagram.LocationItem;
import com.ocd.hashtagsurfer.model.instagram.NearbySearchResponse;
import com.ocd.hashtagsurfer.model.instagram.SearchResponse;
import com.ocd.hashtagsurfer.stream.StreamFragment;
import com.ocd.hashtagsurfer.stream.SurferActivity;
import com.ocd.hashtagsurfer.util.Constants;
import com.ocd.hashtagsurfer.util.Parser;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.util.Iterator;
import java.util.List;

/**
 * Created by rakz on 25/01/15 Copyright OCDTeam.
 */
public class NearbyService extends AbstractInstagramService {

    private static final String LOG_TAG = NearbyService.class.getSimpleName();

    private static final String SEARCH_LOCATION_1_PREFIX = "https://api.instagram.com/v1/locations/search?lat=";
    private static final String SEARCH_LOCATION_2_LNG = "&lng=";
    private static final String SEARCH_LOCATION_3_SUFFIX = "&distance=2000&access_token=";

    private static final String MEDIA_RECENT_FOR_LOCATION_1_PREFIX = "https://api.instagram.com/v1/locations/";
    private static final String MEDIA_RECENT_FOR_LOCATION_2_SUFFIX = "/media/recent?access_token=";

    public NearbyService() {
        super(LOG_TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        if (!intent.hasExtra(SurferActivity.REFRESH_TYPE_KEY)
                || !intent.hasExtra(StreamFragment.LAT_KEY)
                || !intent.hasExtra(StreamFragment.LON_KEY)
                || !intent.hasExtra(SurferActivity.ACCESS_TOKEN_KEY_INSTAGRAM)
                || !intent.hasExtra(StreamFragment.STREAM_RECEIVER)) {
            throw new IllegalArgumentException("Mandatory params missing!");
        }


        NearbySearchResponse nearbySearchResponse = null;
        SearchResponse searchResponse = null;
        int resultCode = Constants.RESULT_CODE_OK;
        HttpURLConnection urlConnection = null;

        try {
            urlConnection = connectToURL(SEARCH_LOCATION_1_PREFIX+intent.getDoubleExtra(StreamFragment.LAT_KEY, 0.0)
                    +SEARCH_LOCATION_2_LNG+intent.getDoubleExtra(StreamFragment.LON_KEY, 0.0)
                    +SEARCH_LOCATION_3_SUFFIX
                    + intent.getStringExtra(SurferActivity.ACCESS_TOKEN_KEY_INSTAGRAM));

            int status = urlConnection.getResponseCode();
            Log.i(LOG_TAG, "Location search returned with status: " + status);
            switch (status) {
                case HttpURLConnection.HTTP_OK:
                    // Parse response
                    nearbySearchResponse = Parser.extractNearbyResults(readStream(urlConnection.getInputStream()));
                    urlConnection.disconnect();
                    List<LocationItem> locations = nearbySearchResponse.getData();
                    if (locations == null || locations.isEmpty()) {
                        Log.e(LOG_TAG, "Failed to get locations, empty!");
                    } else {
//                        Log.i(LOG_TAG, "Nearby search return with: "+locations);
                        for (Iterator<LocationItem> iterator = locations.iterator(); iterator.hasNext();) {
                            LocationItem item = iterator.next();
                            urlConnection = connectToURL(MEDIA_RECENT_FOR_LOCATION_1_PREFIX
                                    +item.getId()
                                    +MEDIA_RECENT_FOR_LOCATION_2_SUFFIX
                                    + intent.getStringExtra(SurferActivity.ACCESS_TOKEN_KEY_INSTAGRAM));
                            status = urlConnection.getResponseCode();
                            switch (status) {
                                case HttpURLConnection.HTTP_OK:
                                    SearchResponse tempResponse = Parser.extractSearchResults(readStream(urlConnection.getInputStream()));
//                                    Log.i(LOG_TAG, "Response for nearby media: "+tempResponse);
                                    if (searchResponse == null) {
                                        searchResponse = tempResponse;
                                    } else {
                                        searchResponse.getData().addAll(tempResponse.getData());
                                    }
                                    urlConnection.disconnect();
                                    break;
                                default:
                                    Log.e(LOG_TAG, "Problem is: " + urlConnection.getResponseMessage() + " ");
                                    if (checkIfExpired(urlConnection)) {
                                        resultCode = Constants.RESULT_CODE_ACCESS_TOKEN_EXPIRED;
                                    }
                                    break;
                            }

                        }
                    }

                    break;
                default:
                    Log.e(LOG_TAG, "Problem is: " + urlConnection.getResponseMessage() + " ");
                    if (checkIfExpired(urlConnection)) {
                        resultCode = Constants.RESULT_CODE_ACCESS_TOKEN_EXPIRED;
                    }
                    break;
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Bad URL", e);
            resultCode = Constants.RESULT_CODE_PROTOCOL_ERROR;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Failed to talk with Instagram", e);
            resultCode = Constants.RESULT_CODE_NETWORK_ERROR;
        } finally {
            ResultReceiver receiver = intent.getParcelableExtra(StreamFragment.STREAM_RECEIVER);
            Bundle result = new Bundle();
            result.putSerializable(SEARCH_RESULT, searchResponse);
// Multiple locations, multiple paginations, for now no reason to pass last one
//            if (searchResponse.getPagination() != null) {
//                result.putString(StreamFragment.NEXT_PAGE_KEY, searchResponse.getPagination().getNext_url());
//            }
            result.putSerializable(SurferActivity.REFRESH_TYPE_KEY, intent.getSerializableExtra(SurferActivity.REFRESH_TYPE_KEY));
            receiver.send(resultCode, result);
        }

    }

}
