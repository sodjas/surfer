package com.ocd.hashtagsurfer.stream;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * Created by rakz on 09/02/15 Copyright OCDTeam.
 */
public class MyGridItem extends RelativeLayout {

    public MyGridItem(Context context, AttributeSet attrs) {
        super(context, attrs, 0);
    }

    public MyGridItem(Context context) {
        super(context);
    }

    @TargetApi(	Build.VERSION_CODES.LOLLIPOP)
    public MyGridItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr, 0);
    }
    @TargetApi(	Build.VERSION_CODES.LOLLIPOP)
    public MyGridItem(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }
}
