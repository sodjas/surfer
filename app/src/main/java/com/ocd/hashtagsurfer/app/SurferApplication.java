package com.ocd.hashtagsurfer.app;

import android.app.Application;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.ocd.hashtagsurfer.R;

/**
 * Created by rakz on 24/01/15 Copyright OCDTeam.
 */
public class SurferApplication extends Application {

    private static Tracker tracker;

    public synchronized Tracker getTracker() {
        if (tracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            tracker = analytics.newTracker(R.xml.global_tracker);
            tracker.enableAdvertisingIdCollection(true);
        }
        return tracker;
    }
}
