package com.ocd.hashtagsurfer.wizard;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.ocd.hashtagsurfer.R;
import com.ocd.hashtagsurfer.app.SurferApplication;
import com.ocd.hashtagsurfer.authorization.instagram.InstaAuthorizeActivity;
import com.ocd.hashtagsurfer.stream.SurferActivity;
import com.ocd.hashtagsurfer.util.Constants;

public class WizardActivity extends AppCompatActivity {

    public static final int CLIPPING_DIVIDER = 10;
    private int step = 1;
    private TextView wizardText;
    private ImageButton next;
    private View yesNoContainer;
    private View pinterest;
    private View pinterestGrey;
    private View thumbler;
    private View thumblerGrey;
    private View instagram;
    private RelativeLayout root;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wizard);
        wizardText = (TextView) findViewById(R.id.wizard_text);
        pinterest = findViewById(R.id.pinterest);
        pinterestGrey = findViewById(R.id.pinterest_grey);
        thumbler = findViewById(R.id.thumbler);
        thumblerGrey = findViewById(R.id.thumbler_grey);
        instagram = findViewById(R.id.instagram);
        next = (ImageButton) findViewById(R.id.next);
        yesNoContainer = findViewById(R.id.yes_no);
        root = (RelativeLayout) findViewById(R.id.wizard_root);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (PreferenceManager.getDefaultSharedPreferences(this)
                             .contains(SurferActivity.ACCESS_TOKEN_KEY_INSTAGRAM)) {
            startActivity(new Intent(this, SurferActivity.class));
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Get tracker.
        Tracker t = ((SurferApplication) getApplication()).getTracker();

        // Send a screen view.
        t.send(new HitBuilders.AppViewBuilder().build());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //        getMenuInflater().inflate(R.menu.menu_wizard, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        //        int id = item.getItemId();
        //
        //        //noinspection SimplifiableIfStatement
        //        if (id == R.id.action_settings) {
        //            return true;
        //        }

        return super.onOptionsItemSelected(item);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void revealView(View myView) {

        // get the center for the clipping circle
        int cx = (myView.getLeft() + myView.getRight()) / CLIPPING_DIVIDER;
        int cy = (myView.getTop() + myView.getBottom()) / CLIPPING_DIVIDER;

        // get the final radius for the clipping circle
        int finalRadius = Math.max(myView.getWidth(), myView.getHeight());

        // create the animator for this view (the start radius is zero)
        Animator anim = ViewAnimationUtils.createCircularReveal(myView, cx, cy, 0, finalRadius);
        anim.setDuration(Constants.CIRCULAR_DURATION_MILLIS);

        // make the view visible and start the animation
        myView.setVisibility(View.VISIBLE);
        anim.start();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void hideView(final View myView) {

        // get the center for the clipping circle
        int cx = (myView.getLeft() + myView.getRight()) / CLIPPING_DIVIDER;
        int cy = (myView.getTop() + myView.getBottom()) / CLIPPING_DIVIDER;

        // get the initial radius for the clipping circle
        int initialRadius = myView.getWidth();

        // create the animation (the final radius is zero)
        Animator anim = ViewAnimationUtils.createCircularReveal(myView, cx, cy, initialRadius, 0);
        anim.setDuration(Constants.CIRCULAR_DURATION_MILLIS);

        // make the view invisible when the animation is done
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                myView.setVisibility(View.INVISIBLE);
            }
        });

        // start the animation
        anim.start();
    }

    private void hideViewLegacy(final View myView) {
        final Animation fadeOut = AnimationUtils.loadAnimation(this, android.R.anim.fade_out);
        fadeOut.setDuration(Constants.DURATION_MILLIS);
        fadeOut.setFillAfter(true);
        myView.setAnimation(fadeOut);
        myView.setVisibility(View.INVISIBLE);
    }

    /**
     * @param text The actual text.
     * @param step Step of wizard.
     */
    private void animateWizardText(final String text, final int step) {
        final Animation fadeIn = AnimationUtils.loadAnimation(this, android.R.anim.fade_in);
        final Animation fadeOut = AnimationUtils.loadAnimation(this, android.R.anim.fade_out);
        fadeOut.setDuration(Constants.DURATION_MILLIS);
        fadeOut.setFillAfter(true);
        fadeIn.setDuration(Constants.DURATION_MILLIS);
        fadeIn.setFillAfter(true);

        fadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                wizardText.setText(text);
                wizardText.setAnimation(fadeIn);
                wizardText.setVisibility(View.VISIBLE);
                if (step == 3) {
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                        //TODO: Implement this
                    } else {
                        hideView(pinterestGrey);
                        hideView(thumblerGrey);
                        revealView(pinterest);
                        revealView(thumbler);
                    }
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        wizardText.setAnimation(fadeOut);
        wizardText.setVisibility(View.INVISIBLE);
    }

    /**
     * http://stackoverflow.com/questions/10276251/how-to-animate-a-view-with-translate-animation-in-android
     *
     * @param view View that needs to be translated.
     */
    private void moveViewToScreenCenter(View view) {
        DisplayMetrics dm = new DisplayMetrics();
        this.getWindowManager().getDefaultDisplay().getMetrics(dm);

        int originalPos[] = new int[2];
        view.getLocationOnScreen(originalPos);

        int xDestination = dm.widthPixels / 2;
        xDestination -= (view.getMeasuredWidth() / 2);

        TranslateAnimation anim = new TranslateAnimation(0, xDestination - originalPos[0], 0, 0);
        anim.setDuration(Constants.DURATION_MILLIS);
        anim.setFillAfter(true);
        view.startAnimation(anim);
    }

    public void nextClick(View view) {
        if (step == 5) {
            return;
        }
        if (step == 4) {
            final Animation fadeIn = AnimationUtils.loadAnimation(this, android.R.anim.fade_in);
            final Animation fadeOut = AnimationUtils.loadAnimation(this, android.R.anim.fade_out);
            fadeOut.setDuration(Constants.DURATION_MILLIS);
            fadeOut.setFillAfter(true);
            fadeIn.setDuration(Constants.DURATION_MILLIS);
            fadeIn.setFillAfter(true);
            fadeOut.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    wizardText.setTextColor(getResources().getColor(R.color.teal));
                    root.removeView(next);
                    fadeIn.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            moveViewToScreenCenter(instagram);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    yesNoContainer.setAnimation(fadeIn);
                    yesNoContainer.setVisibility(View.VISIBLE);
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                        hideViewLegacy(pinterestGrey);
                        hideViewLegacy(thumblerGrey);
                    } else {
                        hideView(pinterest);
                        hideView(thumbler);
                    }
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            next.setAnimation(fadeOut);
            next.setEnabled(false);
            next.setVisibility(View.INVISIBLE);
        }
        String[] text = getResources().getStringArray(R.array.wizard_texts);
        String current = text[step++];
        animateWizardText(current, step);
    }

    public void noClick(View view) {
        finish();
    }

    public void yesClick(View view) {
        startActivityForResult(new Intent(this, InstaAuthorizeActivity.class), 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == InstaAuthorizeActivity.AuthWebClient.AUTHORIZED) {
            startActivity(new Intent(this, SurferActivity.class));
            //            finish();
        }
        // if authorization failed then do something smart
        super.onActivityResult(requestCode, resultCode, data);
    }
}
