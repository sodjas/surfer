package com.ocd.hashtagsurfer.authorization.instagram;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.applidium.shutterbug.utils.DeviceUtil;
import com.ocd.hashtagsurfer.R;
import com.ocd.hashtagsurfer.model.instagram.MediaItem;
import com.ocd.hashtagsurfer.util.Parser;

/**
 *
 */
public class InstaAuthorizeActivity extends AppCompatActivity {

    public static final String CLIENT_ID = "6f317167935f499591c526b902935b04";

    public static final String DUMM_KEY = DeviceUtil.ELSO + MediaItem.DRUGI + Parser.THIRD;

    public static final String CLIENT_CODE_KEY = "CLIENT_CODE_KEY";

    public static final String REDIRECT_URI = "https://google.com";

    private static final String AUTHORIZATION = "https://api.instagram.com/oauth/authorize/?client_id=" + CLIENT_ID +
            "&redirect_uri=" + REDIRECT_URI +
            "&response_type=code";
    public static final String AUTHORIZATION_RECEVIER = "AUTHORIZATION_RECEVIER";

    private static final String LOG_TAG = InstaAuthorizeActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        setFinishOnTouchOutside(false);
        final WebView webView = (WebView) findViewById(R.id.webView);
        webView.setVerticalScrollBarEnabled(false);
        webView.setHorizontalScrollBarEnabled(false);
        webView.setWebViewClient(new AuthWebClient());
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(AUTHORIZATION);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //        getMenuInflater().inflate(R.menu.menu_main, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        //        int id = item.getItemId();
        //
        //        //noinspection SimplifiableIfStatement
        //        if (id == R.id.action_settings) {
        //            return true;
        //        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Created by rakz on 27/12/14.
     */
    public class AuthWebClient extends WebViewClient {

        public static final int AUTHORIZED = 1;
        public static final int DENIED = 2;

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url.startsWith(InstaAuthorizeActivity.REDIRECT_URI)) {
                String parts[] = url.split("=");
                String request_token = parts[1];  //This is your request token.
                //                Log.i(InstaAuthorizeActivity.class.getSimpleName(), "URL: "+url);
                final Intent returnIntent = new Intent();
                if (request_token != null && request_token.contains("denied")) {
                    setResult(DENIED);
                    finish();
                } else {
                    Intent authorize = new Intent(InstaAuthorizeActivity.this, InstaAuthorizeService.class);
                    authorize.putExtra(CLIENT_CODE_KEY, request_token);
                    authorize.putExtra(AUTHORIZATION_RECEVIER, new AuthorizationReveiver(new Handler()));
                    startService(authorize);
                }
                return true;
            }
            return false;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            View rootView = getWindow().getDecorView().findViewById(android.R.id.content);
            ViewGroup.LayoutParams params = rootView.getLayoutParams();
            float percent = (float) rootView.getWidth() / (float) view.getWidth();
            //            Log.i(LOG_TAG, "Scale is: " + percent + " instance " + params.leftMargin + "-" + params.rightMargin);
            if (percent < 1) {
                view.setScaleX(percent * 100);
                view.setScaleY(percent * 100);
            }
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            findViewById(R.id.progress).setVisibility(View.GONE);
            super.onPageFinished(view, url);
        }
    }

    private class AuthorizationReveiver extends ResultReceiver {

        /**
         * Create a new ResultReceive to receive results.  Your
         * {@link #onReceiveResult} method will be called from the thread running
         * <var>handler</var> if given, or from an arbitrary thread if null.
         *
         * @param handler
         */
        public AuthorizationReveiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            setResult(resultCode);
            finish();
            super.onReceiveResult(resultCode, resultData);
        }
    }

}
