package com.ocd.hashtagsurfer.authorization.instagram;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.ResultReceiver;
import android.preference.PreferenceManager;
import android.util.Log;
import android.util.Pair;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocd.hashtagsurfer.stream.SurferActivity;
import com.ocd.hashtagsurfer.util.Parser;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

/**
 *
 */
public class InstaAuthorizeService extends IntentService {

    private static final String AUTHORIZATION_URL = "https://api.instagram.com/oauth/access_token";

    private static final int BUFFER_LENGTH = 200 * 1024;

    private static final int DEFAULT_READ_TIMEOUT = 5000;

    private static final int DEFAULT_CONNECTION_TIMEOUT = 5000;

    private static final String LOG_TAG = InstaAuthorizeService.class.getSimpleName();

    public InstaAuthorizeService() {
        super(LOG_TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        if (intent == null || !intent.hasExtra(InstaAuthorizeActivity.CLIENT_CODE_KEY) || !intent.hasExtra(InstaAuthorizeActivity.AUTHORIZATION_RECEVIER)) {
            throw new IllegalArgumentException("Mandatory params missing!");
        }
        try {

            HttpsURLConnection connection = (HttpsURLConnection) new URL(AUTHORIZATION_URL).openConnection();

            List<Pair<String, String>> params = new ArrayList<Pair<String, String>>();
            params.add(new Pair("client_id", InstaAuthorizeActivity.CLIENT_ID));
            params.add(new Pair("client_secret", InstaAuthorizeActivity.DUMM_KEY));
            params.add(new Pair("grant_type", "authorization_code"));
            params.add(new Pair("redirect_uri", InstaAuthorizeActivity.REDIRECT_URI));
            params.add(new Pair("code", intent.getStringExtra(InstaAuthorizeActivity.CLIENT_CODE_KEY)));

            connection.setUseCaches(false);
            connection.setConnectTimeout(DEFAULT_CONNECTION_TIMEOUT);
            connection.setReadTimeout(DEFAULT_READ_TIMEOUT);
            connection.setRequestMethod("POST");
            connection.setDoInput(true);
            connection.setDoOutput(true);

            final String query = getQuery(params);
            OutputStream os = connection.getOutputStream();
            final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, Charset.defaultCharset()
                                                                                               .displayName()));
            writer.write(query);
            writer.flush();
            writer.close();

            Log.i(LOG_TAG, "Client code authorize returned with status: " + connection.getResponseCode());
            ResultReceiver receiver = intent.getParcelableExtra(InstaAuthorizeActivity.AUTHORIZATION_RECEVIER);
            switch (connection.getResponseCode()) {
                case HttpsURLConnection.HTTP_OK:
                    ObjectMapper mapper = Parser.getObjectMapper();
                    JsonNode jsonNode = mapper.readTree(getContent(connection));
                    String token = jsonNode.get("access_token").textValue();
                    //                    Log.i(LOG_TAG, "Response access_token: " + token);
                    SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString(SurferActivity.ACCESS_TOKEN_KEY_INSTAGRAM, token);
                    editor.commit();
                    receiver.send(InstaAuthorizeActivity.AuthWebClient.AUTHORIZED, null);
                    break;
                default:
                    Log.e(LOG_TAG, "Problem is: " + connection.getResponseMessage());
                    //                    receiver.send(InstaAuthorizeActivity.AuthWebClient.DENIED, null);
                    break;
            }

        } catch (IOException e) {
            Log.e(LOG_TAG, "Failed to authorized with client code", e);
        }

    }

    private String getQuery(final List<Pair<String, String>> params) throws UnsupportedEncodingException {

        final StringBuilder result = new StringBuilder();
        boolean first = true;
        for (final Pair pair : params) {
            if (first) {
                first = false;
            } else {
                result.append("&");
            }
            result.append(URLEncoder.encode((String) pair.first, Charset.defaultCharset()
                                                                        .displayName()));
            result.append("=");
            result.append(URLEncoder.encode((String) pair.second, Charset.defaultCharset()
                                                                         .displayName()));
        }
        return result.toString();

    }

    private String getContent(final HttpURLConnection httpURLConnection) {
        InputStream input = null;
        try {
            final byte[] data = new byte[BUFFER_LENGTH];
            input = httpURLConnection.getInputStream();
            int count = 0;
            int overall = 0;
            while ((count = input.read(data, overall, BUFFER_LENGTH - overall)) != -1 && overall < BUFFER_LENGTH) {
                overall += count;
            }
            return new String(Arrays.copyOf(data, overall));
        } catch (final IOException e) {
            Log.e(getClass().getSimpleName(), "", e);
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (final IOException e) {
                    Log.e(getClass().getSimpleName(), "", e);
                }
            }
        }
        return null;
    }

}
