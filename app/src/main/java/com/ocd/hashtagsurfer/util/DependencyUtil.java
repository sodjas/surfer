/**
 * 
 */
package com.ocd.hashtagsurfer.util;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;

/**
 * Copyright Zoltan Rak, all rights reserved 2013.
 * 
 * @author rakz
 */
public final class DependencyUtil {

	/**
	 * Maps package.
	 */
	public static final String COM_GOOGLE_ANDROID_APPS_MAPS = "com.google.android.apps.maps";

	/**
	 * Maps market url.
	 */
	private static final String MARKET_COM_GOOGLE_ANDROID_APPS_MAPS = "market://details?id=com.google.android.apps.maps";

	/**
	 * Private constructor for utility class.
	 */
	private DependencyUtil() {
	}

	/**
	 * @param context Android application context.
	 * @return whether maps are installed.
	 */
	public static boolean isGoogleMapsInstalled(final Context context) {
		try {
			context.getPackageManager().getApplicationInfo(COM_GOOGLE_ANDROID_APPS_MAPS, 0);
			return true;
		} catch (final PackageManager.NameNotFoundException e) {
			return false;
		}
	}

	/**
	 * @param context Android application context.
	 */
	public static void startGoogleMapsListener(final Context context) {
		final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(MARKET_COM_GOOGLE_ANDROID_APPS_MAPS));
		context.startActivity(intent);
	}

}
