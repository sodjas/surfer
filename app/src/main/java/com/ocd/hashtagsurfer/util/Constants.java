package com.ocd.hashtagsurfer.util;

/**
 * Created by rakz on 02/01/15 Copyright OCDTeam.
 */
public final class Constants {

    public static final int DURATION_MILLIS = 500;
    public static final int CIRCULAR_DURATION_MILLIS = 1000;

    public static final int RESULT_CODE_OK = 0;
    public static final int RESULT_CODE_NETWORK_ERROR = 1;
    public static final int RESULT_CODE_PROTOCOL_ERROR = 2;
    public static final int RESULT_CODE_ACCESS_TOKEN_EXPIRED = 3;
    public static final int RESULT_CODE_GPS_ERROR = 4;
    public static final int RESULT_CODE_EXT_STORAGE_NOT_WIRTABLE = 5;
}
