package com.ocd.hashtagsurfer.util;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ocd.hashtagsurfer.model.instagram.MediaSearchResponse;
import com.ocd.hashtagsurfer.model.instagram.NearbySearchResponse;
import com.ocd.hashtagsurfer.model.instagram.SearchResponse;
import com.ocd.hashtagsurfer.model.instagram.TagSuggestionResponse;
import com.ocd.hashtagsurfer.model.instagram.UserInfo;

import java.io.IOException;

/**
 * Created by rakz on 27/12/14 Copyright OCDTeam.
 */
public final class Parser {

    public static final String THIRD = "fe536d43ef8";
    private static final String TAG_SEARCH_START_OBJECT = "data";
    private static final ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    private Parser() {

    }

    public static ObjectMapper getObjectMapper() {
        return mapper;
    }

    public static SearchResponse extractSearchResults(String response) throws IOException {
        return mapper.readValue(response, SearchResponse.class);
    }

    public static NearbySearchResponse extractNearbyResults(String response) throws IOException {
        return mapper.readValue(response, NearbySearchResponse.class);
    }

    public static MediaSearchResponse extractMediaResults(String response) throws IOException {
        return mapper.readValue(response, MediaSearchResponse.class);
    }

    public static UserInfo extractUserInfo(String response) throws IOException {
        return mapper.readValue(response, UserInfo.class);
    }

    public static TagSuggestionResponse extractTagSuggestionResults(String response) throws IOException {
        return mapper.readValue(response, TagSuggestionResponse.class);
    }
}
