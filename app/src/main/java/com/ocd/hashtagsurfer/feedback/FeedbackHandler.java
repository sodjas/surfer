package com.ocd.hashtagsurfer.feedback;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.Toast;

import com.ocd.hashtagsurfer.R;

public class FeedbackHandler {

//	private static final String LOGTAG = FeedbackHandler.class.getSimpleName();
	
	private static final String TAG_FRAGMENT_NAME = "framgent";

	private Activity context;
	private int containerId;

	public FeedbackHandler(Activity context, int containerId) {
		super();
		this.context = context;
		this.containerId = containerId;
	}

	public void handleFeedback(FeedbackStates feedback) {

		android.app.FragmentManager fm = context.getFragmentManager();
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		Editor edit = prefs.edit();

//		Log.i(LOGTAG, "Enabled: " + context.getResources().getBoolean(R.bool.feedback_enabled));
//
//		if(! context.getResources().getBoolean(R.bool.feedback_enabled)){
//			Log.i(LOGTAG, "Feedback handler has been disabled.");
//			return;
//		}
		
		//if(!FeedbackStates.MAIN_START.equals(feedback)){
			//Tracker.track(context, Tracker.Category.FEEDBACK, Tracker.Action.FEEDBACK_MAIN, feedback.toString());
		//}
		
		switch (feedback) {
			case MAIN_START: {

				int startCountLimit = 3;
				int actualStartCount = prefs.getInt(Const.SharedPrefs.KEY_LAUNCH_COUNT, 0);
				edit.putInt(Const.SharedPrefs.KEY_LAUNCH_COUNT, ++actualStartCount);
				edit.apply();
				boolean neverShow = prefs.getBoolean(Const.Feedback.PREFS_KEY_NEVER_SHOW, false);
				boolean forceShow = prefs.getBoolean(Const.Feedback.PREFS_KEY_FORCE_SHOW, false);
				;
				boolean formFilled = prefs.getBoolean(Const.Feedback.PREFS_KEY_FORM_FILLED, false);
				;

				if ((actualStartCount > startCountLimit && !neverShow && !formFilled) || forceShow) {
					Fragment fragment = fm.findFragmentByTag(TAG_FRAGMENT_NAME);
					if(null == fragment){
						FragmentTransaction t = fm.beginTransaction();
						t.add(containerId,
								new FeedbackQuestion1Fragment(),
								TAG_FRAGMENT_NAME
								);
						t.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
						t.commit();
					}
				}

				break;
			}
			case MAIN_NO: {

				FeedbackQuestion2Fragment fragment = new FeedbackQuestion2Fragment();

				Bundle args = new Bundle();
				args.putInt(FeedbackQuestion2Fragment.ARG_POLL_TYPE,
						FeedbackQuestion2Fragment.POLL_BAD);
				fragment.setArguments(args);

				FragmentTransaction t = fm.beginTransaction();
				t.replace(R.id.main_fragmentcontainer,
						fragment,
						TAG_FRAGMENT_NAME
						);
				t.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
				t.commit();

				break;
			}
			case MAIN_SOMEWHAT: {

				FeedbackQuestion2Fragment fragment = new FeedbackQuestion2Fragment();

				Bundle args = new Bundle();
				args.putInt(FeedbackQuestion2Fragment.ARG_POLL_TYPE,
						FeedbackQuestion2Fragment.POLL_GOOD);
				fragment.setArguments(args);

				FragmentTransaction t = fm.beginTransaction();
				t.replace(R.id.main_fragmentcontainer,
						fragment,
						TAG_FRAGMENT_NAME
						);
				t.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
				t.commit();

				break;
			}
			case MAIN_YES: {

				FragmentTransaction t = fm.beginTransaction();
				t.replace(containerId,
						new FeedbackRatingFragment(),
						TAG_FRAGMENT_NAME
						);
				t.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
				t.commit();

				break;
			}
			case POLL_GOOD_NEVER: 
			case POLL_BAD_NEVER: {

				Toast.makeText(context,
						R.string.feedback_anytime,
						Toast.LENGTH_LONG).show();

				neverShow(prefs);
				removeFragment(fm);

				break;
			}
			case RATING_NOT_NOW:
			case POLL_BAD_NOT_NOW:
			case POLL_GOOD_NOT_NOW: {

				removeFragment(fm);

				break;
			}
			case POLL_BAD_YES: {

				Intent intent = new Intent(context, FeedbackActivity.class);
				intent.putExtra(FeedbackActivity.ARG_POLL, FeedbackActivity.POLL_BAD);
				context.startActivity(intent);
				removeFragment(fm);

				break;
			}
			case POLL_GOOD_YES: {

				Intent intent = new Intent(context, FeedbackActivity.class);
				intent.putExtra(FeedbackActivity.ARG_POLL, FeedbackActivity.POLL_GOOD);
				context.startActivity(intent);
				removeFragment(fm);

				break;
			}
			case RATING_NEVER:

				neverShow(prefs);
				removeFragment(fm);

				break;
			case RATING_YES: {

				Uri uri = Uri.parse("market://details?id=" + context.getPackageName());
				Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
				try {
					context.startActivity(goToMarket);
				} catch (ActivityNotFoundException e) {
					context.startActivity(new Intent(Intent.ACTION_VIEW, Uri
							.parse("http://play.google.com/store/apps/details?id="
									+ context.getPackageName())));
				}

				edit.putBoolean(Const.Feedback.PREFS_KEY_FORM_FILLED, true);
				edit.apply();

				removeFragment(fm);

				break;
			}
			default:
				break;

		}

	}

	private void neverShow(SharedPreferences prefs) {
		Editor edit = prefs.edit();
		edit.putBoolean(Const.Feedback.PREFS_KEY_NEVER_SHOW, true);
		edit.apply();
	}

	private void removeFragment(android.app.FragmentManager fm) {
		FragmentTransaction t = fm.beginTransaction();
		t.remove(fm.findFragmentByTag(TAG_FRAGMENT_NAME));
		t.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		t.commit();
	}

}
