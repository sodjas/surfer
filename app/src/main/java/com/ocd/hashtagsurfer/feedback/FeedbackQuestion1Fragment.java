package com.ocd.hashtagsurfer.feedback;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ocd.hashtagsurfer.R;
import com.ocd.hashtagsurfer.stream.SurferActivity;

public class FeedbackQuestion1Fragment extends Fragment {
	
	public FeedbackQuestion1Fragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getArguments() != null) {
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_feedback_question1, container, false);
		
		rootView.findViewById(R.id.fragment_feedback_q1_yes).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Activity activity = FeedbackQuestion1Fragment.this.getActivity();
				if(activity instanceof SurferActivity){
					((SurferActivity) activity).handleFeedbackAnswer(FeedbackStates.MAIN_YES);
				}
			}
		});
		
		rootView.findViewById(R.id.fragment_feedback_q1_no).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Activity activity = FeedbackQuestion1Fragment.this.getActivity();
				if(activity instanceof SurferActivity){
					((SurferActivity) activity).handleFeedbackAnswer(FeedbackStates.MAIN_NO);
				}
			}
		});
		rootView.findViewById(R.id.fragment_feedback_q1_neutral).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Activity activity = FeedbackQuestion1Fragment.this.getActivity();
				if(activity instanceof SurferActivity){
					((SurferActivity) activity).handleFeedbackAnswer(FeedbackStates.MAIN_SOMEWHAT);
				}
			}
		});
		
		return rootView;
	}

}
