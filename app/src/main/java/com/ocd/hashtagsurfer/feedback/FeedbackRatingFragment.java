package com.ocd.hashtagsurfer.feedback;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ocd.hashtagsurfer.R;
import com.ocd.hashtagsurfer.stream.SurferActivity;

public class FeedbackRatingFragment extends Fragment {
	
	public static String ARG_POLL_TYPE = "ARG_POLL_TYPE";
	public static int POLL_BAD = 0;
	public static int POLL_GOOD = 1;
	
	private int pollType;
	
	public FeedbackRatingFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getArguments() != null) {
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_feedback_rating, container, false);
		
		rootView.findViewById(R.id.fragment_feedback_rating_yes).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Activity activity = FeedbackRatingFragment.this.getActivity();
				if(activity instanceof SurferActivity){
					((SurferActivity) activity).handleFeedbackAnswer(FeedbackStates.RATING_YES);
				}
			}
		});
		rootView.findViewById(R.id.fragment_feedback_rating_never).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Activity activity = FeedbackRatingFragment.this.getActivity();
				if(activity instanceof SurferActivity){
					((SurferActivity) activity).handleFeedbackAnswer(FeedbackStates.RATING_NEVER);
				}
			}
		});
		rootView.findViewById(R.id.fragment_feedback_rating_notnow).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Activity activity = FeedbackRatingFragment.this.getActivity();
				if(activity instanceof SurferActivity){
					((SurferActivity) activity).handleFeedbackAnswer(FeedbackStates.RATING_NOT_NOW);
				}
			}
		});
		
		return rootView;
	}

}
