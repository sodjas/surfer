package com.ocd.hashtagsurfer.feedback;

/** 
 * Feedback states. 
 * @author Adam Zoltan Vegh
 */
public enum FeedbackStates {
	MAIN_START,
	MAIN_YES,
	MAIN_SOMEWHAT,
	MAIN_NO,
	POLL_BAD_YES,
	POLL_BAD_NOT_NOW,
	POLL_BAD_NEVER,
	POLL_GOOD_YES,
	POLL_GOOD_NOT_NOW,
	POLL_GOOD_NEVER,
	RATING_YES,
	RATING_NOT_NOW,
	RATING_NEVER;
}
