package com.ocd.hashtagsurfer.feedback;

import android.app.Activity;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.ocd.hashtagsurfer.R;

public class FeedbackActivity extends AppCompatActivity {

    public static final String ARG_POLL = "ARG_POLL";
    public static final int POLL_BAD = 1;
    public static final int POLL_GOOD = 2;

    private static final String LOGTAG = FeedbackActivity.class.getSimpleName();

    private int pollType;

    private boolean confirmBackPressed;
    private int visits;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        visits = 0;

        super.onCreate(savedInstanceState);
        confirmBackPressed = false;
        getWindow().requestFeature(Window.FEATURE_PROGRESS);

        pollType = getIntent().getExtras().getInt(ARG_POLL);

        setContentView(R.layout.activity_feedback);

        final WebView webview = (WebView) findViewById(R.id.feedback_webview);
        final ProgressBar progress = (ProgressBar) findViewById(R.id.feedback_progress);

        webview.setWebChromeClient(new WebChromeClient() {

            public void onProgressChanged(WebView view, int progress) {
                FeedbackActivity.this.setProgress(progress * 1000);
            }
        });
        webview.setWebViewClient(new WebViewClient() {

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                webview.setVisibility(View.INVISIBLE);
                progress.setVisibility(View.VISIBLE);

            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                Log.i(LOGTAG, "url changed: " + visits + " " + url);

                visits++;

                if (3 < visits) {
                    Toast.makeText(FeedbackActivity.this, "Thank you!", Toast.LENGTH_LONG).show();

                    Editor edit = PreferenceManager.getDefaultSharedPreferences(FeedbackActivity.this)
                                                   .edit();
                    edit.putBoolean(Const.Feedback.PREFS_KEY_FORM_FILLED, true);
                    edit.apply();

                    FeedbackActivity.this.setResult(Activity.RESULT_OK);
                    FeedbackActivity.this.finish();
                }

                webview.getSettings().setJavaScriptEnabled(true);
                webview.loadUrl("javascript:(function(){ " +
                        "css = '#ss-back-button, .ss-password-warning, .ss-footer{ display: none; !important; }';" +
                        "var head = document.getElementsByTagName('head')[0];" +
                        "var s = document.createElement('style');" +
                        "s.setAttribute('type', 'text/css');" +
                        "s.appendChild(document.createTextNode(css));" +
                        "head.appendChild(s);" +
                        "})()");
                webview.getSettings().setJavaScriptEnabled(false);

                webview.postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        webview.setVisibility(View.VISIBLE);
                        progress.setVisibility(View.INVISIBLE);
                    }
                }, 300);

            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(FeedbackActivity.this, "Oh no! " + description, Toast.LENGTH_SHORT)
                     .show();
            }
        });

        String pollUrl = "";

        switch (pollType) {
            case POLL_GOOD:
                pollUrl = "https://docs.google.com/forms/d/1xnyndasLdTG9l5Xd-lK-VoRkXKYO6-d1VRRi98IlTrc/viewform?usp=send_form";
                break;
            case POLL_BAD:
                pollUrl = "https://docs.google.com/forms/d/1MXglHu7UJxPXWZDhpU7GBL182easEXenGGAmOJ2eWx0/viewform?usp=send_form";
                break;

            default:
                break;
        }

        if (!pollUrl.isEmpty()) {
            webview.loadUrl(pollUrl);
        } else {
            setResult(Activity.RESULT_OK);
            finish();
        }

    }

    @Override
    public void onBackPressed() {
        if (confirmBackPressed) {
            super.onBackPressed();
        } else {
            confirmBackPressed = true;
            Toast.makeText(this, "Cancelled!", Toast.LENGTH_LONG).show();
        }
    }

}
