package com.ocd.hashtagsurfer.feedback;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ocd.hashtagsurfer.R;
import com.ocd.hashtagsurfer.stream.SurferActivity;

public class FeedbackQuestion2Fragment extends Fragment {
	
	public static String ARG_POLL_TYPE = "ARG_POLL_TYPE";
	public static int POLL_BAD = 1;
	public static int POLL_GOOD = 2;
	
	private int pollType;
	
	public FeedbackQuestion2Fragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getArguments() != null) {
			pollType = getArguments().getInt(ARG_POLL_TYPE);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_feedback_question2, container, false);
		
		rootView.findViewById(R.id.fragment_feedback_q2_yes).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Activity activity = FeedbackQuestion2Fragment.this.getActivity();
				if(activity instanceof SurferActivity){
					((SurferActivity) activity).handleFeedbackAnswer(
							pollType == POLL_BAD 
									? FeedbackStates.POLL_BAD_YES
									: FeedbackStates.POLL_GOOD_YES
					);
				}
			}
		});
			rootView.findViewById(R.id.fragment_feedback_q2_never).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Activity activity = FeedbackQuestion2Fragment.this.getActivity();
				if(activity instanceof SurferActivity){
					((SurferActivity) activity).handleFeedbackAnswer(
							pollType == POLL_BAD 
									? FeedbackStates.POLL_BAD_NEVER 
									: FeedbackStates.POLL_GOOD_NEVER
					);
				}
			}
		});
		rootView.findViewById(R.id.fragment_feedback_q2_notnow).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Activity activity = FeedbackQuestion2Fragment.this.getActivity();
				if(activity instanceof SurferActivity){
					((SurferActivity) activity).handleFeedbackAnswer(
							pollType == POLL_BAD 
									? FeedbackStates.POLL_BAD_NOT_NOW 
									: FeedbackStates.POLL_GOOD_NOT_NOW
					);
				}
			}
		});
		
		return rootView;
	}

}
