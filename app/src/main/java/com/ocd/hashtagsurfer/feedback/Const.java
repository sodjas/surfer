package com.ocd.hashtagsurfer.feedback;

/**
 * Created by rakz on 23/04/15 Copyright OCDTeam.
 */
public class Const {
    public class Feedback {
        public static  final String PREFS_KEY_FORM_FILLED = "PREFS_KEY_FORM_FILLED";
        public static  final String  PREFS_KEY_NEVER_SHOW = "PREFS_KEY_NEVER_SHOW";
        public static  final String  PREFS_KEY_FORCE_SHOW = "PREFS_KEY_FORCE_SHOW";
    }

    public class SharedPrefs {
        public static  final String KEY_LAUNCH_COUNT = "KEY_LAUNCH_COUNT";
    }
}
