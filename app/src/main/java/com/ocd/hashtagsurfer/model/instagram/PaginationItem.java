package com.ocd.hashtagsurfer.model.instagram;

import java.io.Serializable;

/**
 * Created by rakz on 28/12/14 Copyright OCDTeam.
 */
public class PaginationItem implements Serializable {

    private long next_max_id;
    private long next_max_tag_id;
    private long next_min_id;
    private long min_tag_id;
    private String next_url;

    public long getNext_max_id() {
        return next_max_id;
    }

    public void setNext_max_id(long next_max_id) {
        this.next_max_id = next_max_id;
    }

    public long getNext_max_tag_id() {
        return next_max_tag_id;
    }

    public void setNext_max_tag_id(long next_max_tag_id) {
        this.next_max_tag_id = next_max_tag_id;
    }

    public long getNext_min_id() {
        return next_min_id;
    }

    public void setNext_min_id(long next_min_id) {
        this.next_min_id = next_min_id;
    }

    public long getMin_tag_id() {
        return min_tag_id;
    }

    public void setMin_tag_id(long min_tag_id) {
        this.min_tag_id = min_tag_id;
    }

    public String getNext_url() {
        return next_url;
    }

    public void setNext_url(String next_url) {
        this.next_url = next_url;
    }

    @Override
    public String toString() {
        return "PaginationItem{" +
                "next_max_id=" + next_max_id +
                ", next_max_tag_id=" + next_max_tag_id +
                ", next_min_id=" + next_min_id +
                ", min_tag_id=" + min_tag_id +
                ", next_url='" + next_url + '\'' +
                '}';
    }
}
