package com.ocd.hashtagsurfer.model.instagram;

import java.io.Serializable;

/**
 * Created by rakz on 07/11/15.
 */
public class UserInfo implements Serializable {

    public UserData getData() {
        return data;
    }

    public void setData(final UserData data) {
        this.data = data;
    }

    private UserData data;

}

