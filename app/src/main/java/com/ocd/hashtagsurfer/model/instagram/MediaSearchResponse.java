package com.ocd.hashtagsurfer.model.instagram;

import java.io.Serializable;

/**
 * Created by rakz on 20/02/15 Copyright OCDTeam.
 */
public class MediaSearchResponse implements Serializable {

    private MediaItem data;

    public MediaItem getData() {
        return data;
    }

    public void setData(MediaItem data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "MediaSearchResponse{" +
                "data=" + data +
                '}';
    }
}
