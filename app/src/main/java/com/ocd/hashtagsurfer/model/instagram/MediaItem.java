package com.ocd.hashtagsurfer.model.instagram;

import java.io.Serializable;
import java.util.List;

/**
 * Created by rakz on 28/12/14 Copyright OCDTeam.
 */
public class MediaItem implements Serializable {

    public static final String DRUGI = "bd9416d90cbe";

    public static final String MEDIA_IMAGE_TYPE = "image";

    public static final String MEDIA_VIDEO_TYPE = "video";

    private List<String> tags;

    private String type;

    private String link;

    private UserData user;

    private ImageItem images;

    private VideoItem videos;

    private CaptionItem caption;

    private LikeItem likes;

    private LocationItem location;

    public CaptionItem getCaption() {
        return caption;
    }

    public void setCaption(CaptionItem caption) {
        this.caption = caption;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public ImageItem getImages() {
        return images;
    }

    public void setImages(ImageItem images) {
        this.images = images;
    }

    public VideoItem getVideos() {
        return videos;
    }

    public void setVideos(VideoItem videos) {
        this.videos = videos;
    }

    public UserData getUser() {
        return user;
    }

    public void setUser(UserData user) {
        this.user = user;
    }

    public LikeItem getLikes() {
        return likes;
    }

    public void setLikes(LikeItem likes) {
        this.likes = likes;
    }

    public LocationItem getLocation() {
        return location;
    }

    public void setLocation(LocationItem location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "MediaItem{" +
                "tags=" + tags +
                ", type='" + type + '\'' +
                ", link='" + link + '\'' +
                ", user=" + user +
                ", images=" + images +
                ", videos=" + videos +
                ", caption=" + caption +
                ", likes=" + likes +
                ", location=" + location +
                '}';
    }
}
