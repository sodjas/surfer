package com.ocd.hashtagsurfer.model.instagram;

/**
 * Created by rakz on 22/02/15 Copyright OCDTeam.
 */
public class TagSuggestion {

    private int media_count;

    private String name;

    public int getMedia_count() {
        return media_count;
    }

    public void setMedia_count(int media_count) {
        this.media_count = media_count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "TagSuggestion{" +
                "media_count=" + media_count +
                ", name='" + name + '\'' +
                '}';
    }
}
