package com.ocd.hashtagsurfer.model.instagram;

import java.io.Serializable;
import java.util.List;

/**
 * Created by rakz on 22/02/15 Copyright OCDTeam.
 */
public class TagSuggestionResponse implements Serializable {

    private List<TagSuggestion> data;

    public List<TagSuggestion> getData() {
        return data;
    }

    public void setData(List<TagSuggestion> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "TagSuggestionResponse{" +
                "data=" + data +
                '}';
    }
}
