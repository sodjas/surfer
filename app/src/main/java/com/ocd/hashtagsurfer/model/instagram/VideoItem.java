package com.ocd.hashtagsurfer.model.instagram;

import java.io.Serializable;

/**
 * Created by rakz on 24/01/15 Copyright OCDTeam.
 */
public class VideoItem  implements Serializable {

    private MediaData low_resolution;

    private MediaData standard_resolution;

    public MediaData getStandard_resolution() {
        return standard_resolution;
    }

    public void setStandard_resolution(MediaData standard_resolution) {
        this.standard_resolution = standard_resolution;
    }

    public MediaData getLow_resolution() {
        return low_resolution;
    }

    public void setLow_resolution(MediaData low_resolution) {
        this.low_resolution = low_resolution;
    }

    @Override
    public String toString() {
        return "VideoItem{" +
                "low_resolution=" + low_resolution +
                ", standard_resolution=" + standard_resolution +
                '}';
    }
}
