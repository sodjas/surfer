package com.ocd.hashtagsurfer.model.instagram;

import java.io.Serializable;

/**
 * Created by rakz on 28/12/14 Copyright OCDTeam.
 */
public class CaptionItem implements Serializable {

    private String text;

    private long creationTime;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public long getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(long creationTime) {
        this.creationTime = creationTime;
    }

    @Override
    public String toString() {
        return "CaptionItem{" +
                "text='" + text + '\'' +
                ", creationTime=" + creationTime +
                '}';
    }
}
