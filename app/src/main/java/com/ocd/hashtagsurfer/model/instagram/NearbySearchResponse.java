package com.ocd.hashtagsurfer.model.instagram;

import java.io.Serializable;
import java.util.List;

/**
 * Created by rakz on 25/01/15 Copyright OCDTeam.
 */
public class NearbySearchResponse implements Serializable {

    private List<LocationItem> data;

    public List<LocationItem> getData() {
        return data;
    }

    public void setData(List<LocationItem> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "NearbySearchResponse{" +
                "data=" + data +
                '}';
    }
}
