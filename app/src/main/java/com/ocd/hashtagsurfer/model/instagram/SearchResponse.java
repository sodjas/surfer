package com.ocd.hashtagsurfer.model.instagram;

import java.io.Serializable;
import java.util.List;

/**
 * Created by rakz on 28/12/14 Copyright OCDTeam.
 */
public class SearchResponse implements Serializable {
    private PaginationItem pagination;
    //    private int meta;
    private List<MediaItem> data;

    public PaginationItem getPagination() {
        return pagination;
    }

    public void setPagination(PaginationItem pagination) {
        this.pagination = pagination;
    }
//
//    public int getMeta() {
//        return meta;
//    }
//
//    public void setMeta(int meta) {
//        this.meta = meta;
//    }

    public List<MediaItem> getData() {
        return data;
    }

    public void setData(List<MediaItem> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "SearchResponse{" +
                "pagination=" + pagination +
//                ", meta='" + meta + '\'' +
                ", data=" + data +
                '}';
    }
}
