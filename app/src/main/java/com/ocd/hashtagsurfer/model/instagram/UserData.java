package com.ocd.hashtagsurfer.model.instagram;

import java.io.Serializable;

/**
 * Created by rakz on 03/01/15 Copyright OCDTeam.
 */
public class UserData implements Serializable {
    private String username;
    private String full_name;
    private String profile_picture;
    private String bio;
    private String website;
    private long id;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getProfile_picture() {
        return profile_picture;
    }

    public void setProfile_picture(String profile_picture) {
        this.profile_picture = profile_picture;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "UserData{" +
                "username='" + username + '\'' +
                ", full_name='" + full_name + '\'' +
                ", profile_picture='" + profile_picture + '\'' +
                ", bio='" + bio + '\'' +
                ", website='" + website + '\'' +
                ", id=" + id +
                '}';
    }
}
