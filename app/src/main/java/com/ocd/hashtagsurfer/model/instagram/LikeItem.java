package com.ocd.hashtagsurfer.model.instagram;

import java.io.Serializable;

/**
 * Created by rakz on 24/01/15 Copyright OCDTeam.
 */
public class LikeItem implements Serializable {

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    private int count;

    @Override
    public String toString() {
        return "LikeItem{" +
                "count=" + count +
                '}';
    }
}
