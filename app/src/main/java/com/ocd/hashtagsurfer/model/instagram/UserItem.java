package com.ocd.hashtagsurfer.model.instagram;

import java.io.Serializable;

/**
 * Created by rakz on 28/12/14 Copyright OCDTeam.
 */
public class UserItem implements Serializable {
    private String username;
    private String profile_picture;
    private String full_name;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getProfile_picture() {
        return profile_picture;
    }

    public void setProfile_picture(String profile_picture) {
        this.profile_picture = profile_picture;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    @Override
    public String toString() {
        return "UserItem{" +
                "username='" + username + '\'' +
                ", profile_picture='" + profile_picture + '\'' +
                ", full_name='" + full_name + '\'' +
                '}';
    }
}
