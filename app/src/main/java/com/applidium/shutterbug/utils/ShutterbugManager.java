package com.applidium.shutterbug.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ProgressBar;

import com.applidium.shutterbug.cache.DiskLruCache.Snapshot;
import com.applidium.shutterbug.cache.ImageCache;
import com.applidium.shutterbug.cache.ImageCache.ImageCacheListener;
import com.applidium.shutterbug.downloader.ShutterbugDownloader;
import com.applidium.shutterbug.downloader.ShutterbugDownloader.ShutterbugDownloaderListener;
import com.ocd.hashtagsurfer.model.instagram.MediaItem;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShutterbugManager implements ImageCacheListener, ShutterbugDownloaderListener {
    final static private int LISTENER_NOT_FOUND = -1;
    public static final String LOG_TAG = ShutterbugManager.class.getSimpleName();

    private static ShutterbugManager sImageManager;

    private Context mContext;
    private List<String> mFailedUrls = new ArrayList<>();
    private List<ShutterbugManagerListener> mCacheListeners = new ArrayList<>();
    private List<String> mCacheUrls = new ArrayList<>();
    private Map<String, ShutterbugDownloader> mDownloadersMap = new HashMap<>();
//    private List<DownloadRequest> mDownloadRequests = new ArrayList<>();
    private List<ShutterbugManagerListener> mDownloadImageListeners = new ArrayList<>();
    private List<ShutterbugDownloader> mDownloaders = new ArrayList<>();

    public ShutterbugManager(Context context) {
        mContext = context;
    }

    public static ShutterbugManager getSharedImageManager(Context context) {
        if (sImageManager == null) {
            sImageManager = new ShutterbugManager(context);
        }
        return sImageManager;
    }

    public void download(String url, int x, int y, ShutterbugManagerListener listener) {

        if (url == null || listener == null) {
            //rakz modify we want to retry failed urls
            //if (url == null || listener == null || mFailedUrls.contains(url)) {
            return;
        }
        if (mFailedUrls.contains(url)) {
            mFailedUrls.remove(url);
        }

        mCacheListeners.add(listener);
        mCacheUrls.add(url);
//        Log.i(LOG_TAG, "Cachelisternes: "+mCacheListeners.size() +" Cacheurls: "+mCacheUrls.size());
        ImageCache.getSharedImageCache(mContext).queryCache(getCacheKey(url), this, new DownloadRequest(url, x, y, listener));
    }

    public String getCacheKey(String url) {
        try {
            // rakz mod form US-ASCII to UTF-8
            return URLEncoder.encode(url, Charset.defaultCharset().displayName());
        } catch (UnsupportedEncodingException e) {
            Log.e(LOG_TAG, "Failed to encode url", e);
            return null;
        }
    }

    public String getCacheFilePathForOneValueCount(String url) {
        try {
            // rakz mod form US-ASCII to UTF-8
            return URLEncoder.encode(url, Charset.defaultCharset().displayName())+".0";
        } catch (UnsupportedEncodingException e) {
            Log.e(LOG_TAG, "Failed to encode url", e);
            return null;
        }
    }

    private int getListenerIndex(ShutterbugManagerListener listener, String url) {
        for (int index = 0; index < mCacheListeners.size(); index++) {
            if (mCacheListeners.get(index) == listener && mCacheUrls.get(index).equals(url)) {
                return index;
            }
        }
        return LISTENER_NOT_FOUND;
    }

    @Override
    public void onImageFound(Bitmap bitmap, String key, DownloadRequest downloadRequest) {
        final String url = downloadRequest.getUrl();
        final ShutterbugManagerListener listener = downloadRequest.getListener();

        int idx = getListenerIndex(listener, url);
//        Log.i(LOG_TAG, "onImageFound called on "+idx);
        if (idx == LISTENER_NOT_FOUND) {
            // Request has since been canceled
            return;
        }

        listener.onImageSuccess(bitmap, url);
        downloadRequest.clearListener();
        mCacheListeners.remove(idx);
        mCacheUrls.remove(idx);
    }

    @Override
    public void onImageNotFound(String key, DownloadRequest downloadRequest) {
        final String url = downloadRequest.getUrl();
        final ShutterbugManagerListener listener = downloadRequest.getListener();

        int idx = getListenerIndex(listener, url);
//        Log.i(LOG_TAG, "onImageNotFound called on "+idx);
        if (idx == LISTENER_NOT_FOUND) {
            // Request has since been canceled
            return;
        }
        mCacheListeners.remove(idx);
        mCacheUrls.remove(idx);

        // Share the same downloader for identical URLs so we don't download the
        // same URL several times
        ShutterbugDownloader downloader = mDownloadersMap.get(url);
        if (downloader == null) {
            downloader = new ShutterbugDownloader(mContext, url, this, downloadRequest);
            downloader.start();
            mDownloadersMap.put(url, downloader);
        }
//        mDownloadRequests.add(downloadRequest);
        mDownloadImageListeners.add(listener);
        mDownloaders.add(downloader);
//        Log.i(LOG_TAG,
//                " mDownloadImageListeners: "+mDownloadImageListeners.size() +
//                " mDownloaders: "+mDownloaders.size());
    }

    @Override
    public void onImageDownloadSuccess(final ShutterbugDownloader downloader, final InputStream inputStream,
                                       final DownloadRequest downloadRequest) {
        new InputStreamHandlingTask(downloader, downloadRequest).execute(inputStream);
    }

    @Override
    public void onImageDownloadFailure(ShutterbugDownloader downloader, DownloadRequest downloadRequest) {
        for (int idx = mDownloaders.size() - 1; idx >= 0; idx--) {
            final int uidx = idx;
            ShutterbugDownloader aDownloader = mDownloaders.get(uidx);
            if (aDownloader == downloader) {
                ShutterbugManagerListener listener = mDownloadImageListeners.get(uidx);
                listener.onImageFailure(downloadRequest.getUrl());
                mDownloaders.remove(uidx);
                mDownloadImageListeners.remove(uidx);
                Log.e(LOG_TAG, "onImageDownloadFailure called on "+uidx);
            }

        }
        downloadRequest.clearListener();
        mDownloadersMap.remove(downloadRequest.getUrl());
    }

    public void cancel(ShutterbugManagerListener listener) {
        int idx;
        while ((idx = mCacheListeners.indexOf(listener)) != -1) {
            mCacheListeners.remove(idx);
            mCacheUrls.remove(idx);
        }

        while ((idx = mDownloadImageListeners.indexOf(listener)) != -1) {
            ShutterbugDownloader downloader = mDownloaders.get(idx);
            Log.e(LOG_TAG, "cancel called on "+idx);
//            mDownloadRequests.remove(idx);
            mDownloadImageListeners.remove(idx);
            mDownloaders.remove(idx);

            if (!mDownloaders.contains(downloader)) {
                // No more listeners are waiting for this download, cancel it
                downloader.cancel();
                mDownloadersMap.remove(downloader.getUrl());
            }
        }

    }

    public interface ShutterbugManagerListener {
        void onImageSuccess(Bitmap bitmap, String url);

        void onImageFailure(String url);
    }

    public interface ShutterbugManagerPreLoadListener {
        void onImageSuccess(MediaItem item, ProgressBar progressBar);

        void onImageFailure();
    }

    private class InputStreamHandlingTask extends AsyncTask<InputStream, Void, Bitmap> {
        ShutterbugDownloader mDownloader;
        DownloadRequest mDownloadRequest;

        InputStreamHandlingTask(ShutterbugDownloader downloader, DownloadRequest downloadRequest) {
            mDownloader = downloader;
            mDownloadRequest = downloadRequest;
        }

        @Override
        protected Bitmap doInBackground(InputStream... params) {
            final ImageCache sharedImageCache = ImageCache.getSharedImageCache(mContext);
            final String cacheKey = getCacheKey(mDownloadRequest.getUrl());
            // Store the image in the cache
            Snapshot cachedSnapshot = sharedImageCache.storeToDisk(params[0], cacheKey);
            Bitmap bitmap = null;
            if (cachedSnapshot != null) {
                try {
                    bitmap = BitmapUtil.decodeSampledBitmapFromInputStream(cachedSnapshot.getInputStream(0), mDownloadRequest.getX(),  mDownloadRequest.getY());
                } catch (OutOfMemoryError e) {
                    Log.e(LOG_TAG, "Got out of memory error", e);
                } catch (IOException e) {
                    Log.e(LOG_TAG, "Got IOException", e);
                }
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            // Notify all the downloadListener with this downloader
            for (int idx = mDownloaders.size() - 1; idx >= 0; idx--) {
                final int uidx = idx;
                ShutterbugDownloader aDownloader = mDownloaders.get(uidx);
                if (aDownloader == mDownloader) {
                    ShutterbugManagerListener listener = mDownloadImageListeners.get(uidx);
                    if (bitmap != null) {
                        listener.onImageSuccess(bitmap, mDownloadRequest.getUrl());
                    } else {
                        listener.onImageFailure(mDownloadRequest.getUrl());
                    }
                    mDownloaders.remove(uidx);
                    mDownloadImageListeners.remove(uidx);
                }
            }
            if (bitmap != null) {
            } else { // TODO add retry option
                // rakz mode we want to retry failed urls
                //mFailedUrls.add(mDownloadRequest.getUrl());
            }
            mDownloadRequest.clearListener();
            mDownloadersMap.remove(mDownloadRequest.getUrl());
        }

    }
}
