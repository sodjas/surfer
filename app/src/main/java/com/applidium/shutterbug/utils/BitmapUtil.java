package com.applidium.shutterbug.utils;


import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Bitmap util class.
 *
 * @author ddani
 */
public final class BitmapUtil {

    /**
     * Input stream read limit. Images up to 300K are supported.
     */
    private static final int READ_LIMIT = 500 * 1024;

    /**
     * Private constructor.
     */
    private BitmapUtil() {

    }

    /**
     * Calculate sample size.
     *
     * @param options   {@link BitmapFactory.Options}
     * @param reqWidth  int bitmap width
     * @param reqHeight int bitmap height
     * @return sample size
     */
    public static int calculateInSampleSize(final BitmapFactory.Options options, final int reqWidth, final int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;

        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                return Math.round((float) height / (float) reqHeight);
            } else if (width < height) {
                return Math.round((float) width / (float) reqWidth);
            }
        }
        return 1;
    }

    /**
     * Decode bitmap from resource.
     *
     * @param res       The resources object containing the image data
     * @param resId     The resource id of the image data
     * @param reqWidth  int bitmap width
     * @param reqHeight int bitmap height
     * @return bitmap {@link Bitmap}
     */
    public static Bitmap decodeSampledBitmapFromResource(final Resources res, final int resId, final int reqWidth, final int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    /**
     * Decode bitmap from base 64 string.
     *
     * @param base64    The base64 string object containing the image data
     * @param reqWidth  int bitmap width
     * @param reqHeight int bitmap height
     * @return bitmap {@link Bitmap}
     */
    public static Bitmap decodeSampledBitmapFromBase64String(final String base64, final int reqWidth, final int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

        final byte[] photo = Base64.decode(base64, Base64.DEFAULT);
        BitmapFactory.decodeByteArray(photo, 0, photo.length, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeByteArray(photo, 0, photo.length, options);
    }

    /**
     * Decode bitmap from input stream.
     *
     * @param inputStream The input stream that holds the raw data to be decoded into a bitmap
     * @param reqWidth    int bitmap width
     * @param reqHeight   int bitmap height
     * @return bitmap {@link Bitmap}
     * @throws IOException if this stream is closed, no mark has been set or the mark is no longer valid because more than readlimit bytes
     *                     have been read since setting the mark
     */
    public static Bitmap decodeSampledBitmapFromInputStream(final InputStream inputStream, final int reqWidth, final int reqHeight)
            throws IOException {
        final BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream, READ_LIMIT);
        try {
            final BitmapFactory.Options options = new BitmapFactory.Options();
            // For further memory savings, you may want to consider using this option
            // decodeBitmapOptions.inPreferredConfig = Config.RGB_565; // Uses 2-bytes instead of default 4 per pixel

            options.inJustDecodeBounds = true;
            // 32k is probably overkill, but 8k is insufficient for some jpgs
            bufferedInputStream.mark(READ_LIMIT);
            BitmapFactory.decodeStream(bufferedInputStream, null, options);
            bufferedInputStream.reset();

            // inSampleSize prefers multiples of 2, but we prefer to prioritize memory savings
//            Log.i(BitmapUtil.class.getSimpleName(), "Req dimen "+reqWidth+"x"+reqHeight);
            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
            options.inJustDecodeBounds = false;

            return BitmapFactory.decodeStream(bufferedInputStream, null, options);
        } finally {
            try {
                bufferedInputStream.close();
            } catch (final IOException ignored) {
                Log.e("BitmapUtil", "IOException", ignored);
            }
        }

    }

    /**
     * Decode bitmap from byte array.
     *
     * @param data      The byte array object containing the image data
     * @param reqWidth  int bitmap width
     * @param reqHeight int bitmap height
     * @return bitmap {@link Bitmap}
     */
    public static Bitmap decodeSampledBitmapFromByteArray(final byte[] data, final int reqWidth, final int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

        BitmapFactory.decodeByteArray(data, 0, data.length, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeByteArray(data, 0, data.length, options);
    }

}