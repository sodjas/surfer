package com.applidium.shutterbug.utils;

import com.applidium.shutterbug.utils.ShutterbugManager.ShutterbugManagerListener;

public class DownloadRequest {
    private String mUrl;
    private ShutterbugManagerListener mListener;

    int x, y;

    public DownloadRequest(String url, int x, int y, ShutterbugManagerListener listener) {
        mUrl = url;
        mListener = listener;
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public String getUrl() {
        return mUrl;
    }

    public ShutterbugManagerListener getListener() {
        return mListener;
    }

    public void clearListener() {
        mListener = null;
    }
}
