package com.applidium.shutterbug.utils;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.view.Display;
import android.view.WindowManager;


/**
 * Device util.
 *
 * @author ddani
 */
public final class DeviceUtil {

    /**
     * Https scheme.
     */
    public static final String HTTPS_SCHEME = "https";
    /**
     * Http scheme.
     */
    public static final String HTTP_SCHEME = "http";

    /**
     * Keystore type.
     */
    public static final String BKS_TYPE = "BKS";
    /**
     * Radix value for hash computation.
     */
    public static final int RADIX = 16;
    /**
     * Constant for hash computation.
     */
    public static final int HEX_100 = 0x100;
    /**
     * Mask for hash computation.
     */
    public static final int FULL_MASK = 0xff;

    public static final String ELSO = "33251e28b";

    /**
     * Private constructor.
     */
    private DeviceUtil() {
    }

    /**
     * Gets the device display size.
     *
     * @param context Context
     * @return Point
     */
    @SuppressWarnings("deprecation")
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public static Point getDisplaySize(final Context context) {
        final WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        final Display display = windowManager.getDefaultDisplay();
        final Point point = new Point();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            display.getSize(point);
        } else {
            point.x = display.getWidth();
            point.y = display.getHeight();
        }
        return point;
    }

}