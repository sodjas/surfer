package com.applidium.shutterbug;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.applidium.shutterbug.utils.ShutterbugManager;
import com.applidium.shutterbug.utils.ShutterbugManager.ShutterbugManagerListener;

public class FetchableImageView extends ImageView implements ShutterbugManagerListener {

    private FetchableImageViewListener mListener;

    public FetchableImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FetchableImageViewListener getListener() {
        return mListener;
    }

    public void setListener(FetchableImageViewListener listener) {
        mListener = listener;
    }

    public void setImage(String url, int x, int y) {
        final ShutterbugManager manager = ShutterbugManager.getSharedImageManager(getContext());
        manager.cancel(this);
        setImageBitmap(null);
        if (url != null) {
            manager.download(url, x, y, this);
        }
    }
//
//    public void cancelCurrentImageLoad() {
//        ShutterbugManager.getSharedImageManager(getContext()).cancel(this);
//    }

    @Override
    public void onImageSuccess(Bitmap bitmap, String url) {
        setImageBitmap(bitmap);
//        final Animation animation = AnimationUtils.loadAnimation(getContext(), android.R.anim.fade_in);
//        setAnimation(animation);
        if (mListener != null) {
            mListener.onImageFetched(bitmap, url);
        }
    }

    @Override
    public void onImageFailure(String url) {
        if (mListener != null) {
            mListener.onImageFailure(url);
        }
    }

    public interface FetchableImageViewListener {
        void onImageFetched(Bitmap bitmap, String url);
        void onImageFailure(String url);
    }

}
