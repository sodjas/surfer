package com.applidium.shutterbug.downloader;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.applidium.shutterbug.utils.DownloadRequest;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ShutterbugDownloader {
    private final static int TIMEOUT = 4000;
    private static final int THREAD_POOL = 40;
    private static final ExecutorService downloaderThreadService = Executors.newFixedThreadPool(THREAD_POOL);
    public static final String LOG_TAG = ShutterbugDownloader.class.getSimpleName();
    private String mUrl;
    private ShutterbugDownloaderListener mListener;
    private DownloadRequest mDownloadRequest;
    private AsyncTask<Void, Void, InputStream> mCurrentTask;

    public ShutterbugDownloader(Context context, String url, ShutterbugDownloaderListener listener, DownloadRequest downloadRequest) {
        mUrl = url;
        mListener = listener;
        mDownloadRequest = downloadRequest;
    }

    public String getUrl() {
        return mUrl;
    }

    public ShutterbugDownloaderListener getListener() {
        return mListener;
    }

    public DownloadRequest getDownloadRequest() {
        return mDownloadRequest;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void start() {
        mCurrentTask = new AsyncTask<Void, Void, InputStream>() {

            @Override
            protected InputStream doInBackground(Void... params) {
                //                Log.i(LOG_TAG, "Downloading on thread "+hashCode()+" from: "+mUrl);
                try {
                    URL imageUrl = new URL(mUrl);
                    HttpURLConnection connection = (HttpURLConnection) imageUrl.openConnection();
                    connection.setConnectTimeout(TIMEOUT);
                    connection.setReadTimeout(TIMEOUT);
                    InputStream inputStream = connection.getInputStream();
                    return inputStream;
                } catch (IOException e) {
                    Log.e(LOG_TAG, "Failed to get istream", e);
                }
                return null;
            }

            @Override
            protected void onPostExecute(InputStream inputStream) {
                if (isCancelled()) {
                    inputStream = null;
                }

                if (inputStream != null) {
                    mListener.onImageDownloadSuccess(ShutterbugDownloader.this, inputStream, mDownloadRequest);
                } else {
                    mListener.onImageDownloadFailure(ShutterbugDownloader.this, mDownloadRequest);
                }
            }

        };

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
            mCurrentTask.execute();
        } else {
            mCurrentTask.executeOnExecutor(downloaderThreadService, new Void[0]);
        }

    }

    public void cancel() {
        if (mCurrentTask != null) {
            mCurrentTask.cancel(true);
        }
    }

    public interface ShutterbugDownloaderListener {
        void onImageDownloadSuccess(ShutterbugDownloader downloader, InputStream inputStream, DownloadRequest downloadRequest);

        void onImageDownloadFailure(ShutterbugDownloader downloader, DownloadRequest downloadRequest);
    }
}
