-libraryjars ./libs/commons-compress-1.9.jar
-libraryjars ./libs/commons-io-2.4.jar
-libraryjars ./libs/jackson-annotations-2.4.0.jar
-libraryjars ./libs/jackson-core-2.4.2.jar
-libraryjars ./libs/jackson-databind-2.4.2.jar

-optimizationpasses 5
-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-dontpreverify
-verbose
#-dontobfuscate
-optimizations !code/simplification/arithmetic,!field/*,!class/merging/*,!code/allocation/variable

# The support library contains references to newer platform versions.
# Don't warn about those in case this app is linking against an older
# platform version.  We know about them, and they are safe.
-dontwarn android.support.**

-dontwarn sun.misc.Unsafe 
-dontwarn org.w3c.dom.**
-dontwarn org.joda.**
-dontwarn com.google.common.**

-keepattributes *Annotation*,EnclosingMethod,LocalVariableTable,LocalVariableTypeTable,Signature

-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider
-keep public class * extends android.app.backup.BackupAgentHelper
-keep public class * extends android.preference.Preference
-keep public class * extends com.google.android.gcm.GCMBaseIntentService
-keep public class com.android.vending.licensing.ILicensingService

-keepclasseswithmembernames class * {
    native <methods>;
}

#To remove debug logs:
#    public static *** e(...);
-assumenosideeffects class android.util.Log {
    public static *** d(...);
    public static *** v(...);
    public static *** i(...);
    public static *** w(...);
}

-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet);
}

-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet, int);
}

-keep public class * extends android.view.View {
    public <init>(android.content.Context);
    public <init>(android.content.Context, android.util.AttributeSet);
    public <init>(android.content.Context, android.util.AttributeSet, int);
    public void set*(...);
}

-keepclassmembers class * extends android.app.Activity {
   public void *(android.view.View);
}

-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

-keep class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator *;
}

-keepclassmembers class **.R$* {
    public static <fields>;
}

#Objectmapper also works with members so it is kept
-keep class org.codehaus.jackson.** {
  *;
}


-keep class com.ocd.hashtagsurfer.model.instagram.** {
  *;
}
